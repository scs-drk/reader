webpackJsonp(["main"],{

/***/ "../../../../../src/$$_lazy_route_resource lazy recursive":
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncatched exception popping up in devtools
	return Promise.resolve().then(function() {
		throw new Error("Cannot find module '" + req + "'.");
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = "../../../../../src/$$_lazy_route_resource lazy recursive";

/***/ }),

/***/ "../../../../../src/app/app.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/app.component.html":
/***/ (function(module, exports) {

module.exports = "<app-navbar></app-navbar>\n<div class=\"container-fluid\">\n  <flash-messages></flash-messages>\n  <router-outlet></router-outlet>\n<div>\n"

/***/ }),

/***/ "../../../../../src/app/app.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

var AppComponent = (function () {
    function AppComponent() {
        this.title = 'app';
    }
    AppComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-root',
            template: __webpack_require__("../../../../../src/app/app.component.html"),
            styles: [__webpack_require__("../../../../../src/app/app.component.css")]
        })
    ], AppComponent);
    return AppComponent;
}());



/***/ }),

/***/ "../../../../../src/app/app.module.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__ = __webpack_require__("../../../platform-browser/esm5/platform-browser.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_forms__ = __webpack_require__("../../../forms/esm5/forms.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_router__ = __webpack_require__("../../../router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__angular_http__ = __webpack_require__("../../../http/esm5/http.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__app_component__ = __webpack_require__("../../../../../src/app/app.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__components_navbar_navbar_component__ = __webpack_require__("../../../../../src/app/components/navbar/navbar.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__components_login_login_component__ = __webpack_require__("../../../../../src/app/components/login/login.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__components_register_register_component__ = __webpack_require__("../../../../../src/app/components/register/register.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__components_home_home_component__ = __webpack_require__("../../../../../src/app/components/home/home.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__components_dashboard_dashboard_component__ = __webpack_require__("../../../../../src/app/components/dashboard/dashboard.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__components_profile_profile_component__ = __webpack_require__("../../../../../src/app/components/profile/profile.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__components_picker_picker_component__ = __webpack_require__("../../../../../src/app/components/picker/picker.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__components_feed_feed_component__ = __webpack_require__("../../../../../src/app/components/feed/feed.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14__components_categories_categories_component__ = __webpack_require__("../../../../../src/app/components/categories/categories.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15__components_sources_sources_component__ = __webpack_require__("../../../../../src/app/components/sources/sources.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_16__services_validate_service__ = __webpack_require__("../../../../../src/app/services/validate.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_17__services_auth_service__ = __webpack_require__("../../../../../src/app/services/auth.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_18__services_data_service__ = __webpack_require__("../../../../../src/app/services/data.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_19_angular2_flash_messages__ = __webpack_require__("../../../../angular2-flash-messages/module/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_19_angular2_flash_messages___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_19_angular2_flash_messages__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_20__guards_auth_guard__ = __webpack_require__("../../../../../src/app/guards/auth.guard.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};





















var appRoutes = [
    { path: '', component: __WEBPACK_IMPORTED_MODULE_9__components_home_home_component__["a" /* HomeComponent */], pathMatch: 'full' },
    { path: 'register', component: __WEBPACK_IMPORTED_MODULE_8__components_register_register_component__["a" /* RegisterComponent */], pathMatch: 'full' },
    { path: 'login', component: __WEBPACK_IMPORTED_MODULE_7__components_login_login_component__["a" /* LoginComponent */], pathMatch: 'full' },
    { path: 'feed', component: __WEBPACK_IMPORTED_MODULE_13__components_feed_feed_component__["a" /* FeedComponent */], pathMatch: 'full', canActivate: [__WEBPACK_IMPORTED_MODULE_20__guards_auth_guard__["a" /* AuthGuard */]] },
    { path: 'dashboard', component: __WEBPACK_IMPORTED_MODULE_10__components_dashboard_dashboard_component__["a" /* DashboardComponent */], pathMatch: 'full', canActivate: [__WEBPACK_IMPORTED_MODULE_20__guards_auth_guard__["a" /* AuthGuard */]] },
    { path: 'profile', component: __WEBPACK_IMPORTED_MODULE_11__components_profile_profile_component__["a" /* ProfileComponent */], pathMatch: 'full', canActivate: [__WEBPACK_IMPORTED_MODULE_20__guards_auth_guard__["a" /* AuthGuard */]] },
    { path: 'picker', component: __WEBPACK_IMPORTED_MODULE_12__components_picker_picker_component__["a" /* PickerComponent */], pathMatch: 'full' },
    { path: 'categories', component: __WEBPACK_IMPORTED_MODULE_14__components_categories_categories_component__["a" /* CategoriesComponent */], pathMatch: 'full', canActivate: [__WEBPACK_IMPORTED_MODULE_20__guards_auth_guard__["a" /* AuthGuard */]] },
    { path: 'sources', component: __WEBPACK_IMPORTED_MODULE_15__components_sources_sources_component__["a" /* SourcesComponent */], pathMatch: 'full', canActivate: [__WEBPACK_IMPORTED_MODULE_20__guards_auth_guard__["a" /* AuthGuard */]] }
];
var AppModule = (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_5__app_component__["a" /* AppComponent */],
                __WEBPACK_IMPORTED_MODULE_6__components_navbar_navbar_component__["a" /* NavbarComponent */],
                __WEBPACK_IMPORTED_MODULE_7__components_login_login_component__["a" /* LoginComponent */],
                __WEBPACK_IMPORTED_MODULE_8__components_register_register_component__["a" /* RegisterComponent */],
                __WEBPACK_IMPORTED_MODULE_9__components_home_home_component__["a" /* HomeComponent */],
                __WEBPACK_IMPORTED_MODULE_10__components_dashboard_dashboard_component__["a" /* DashboardComponent */],
                __WEBPACK_IMPORTED_MODULE_11__components_profile_profile_component__["a" /* ProfileComponent */],
                __WEBPACK_IMPORTED_MODULE_12__components_picker_picker_component__["a" /* PickerComponent */],
                __WEBPACK_IMPORTED_MODULE_13__components_feed_feed_component__["a" /* FeedComponent */],
                __WEBPACK_IMPORTED_MODULE_14__components_categories_categories_component__["a" /* CategoriesComponent */],
                __WEBPACK_IMPORTED_MODULE_15__components_sources_sources_component__["a" /* SourcesComponent */]
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__["a" /* BrowserModule */],
                __WEBPACK_IMPORTED_MODULE_2__angular_forms__["a" /* FormsModule */],
                __WEBPACK_IMPORTED_MODULE_3__angular_router__["b" /* RouterModule */].forRoot(appRoutes),
                __WEBPACK_IMPORTED_MODULE_19_angular2_flash_messages__["FlashMessagesModule"].forRoot(),
                __WEBPACK_IMPORTED_MODULE_4__angular_http__["HttpModule"]
            ],
            providers: [__WEBPACK_IMPORTED_MODULE_16__services_validate_service__["a" /* ValidateService */], __WEBPACK_IMPORTED_MODULE_17__services_auth_service__["a" /* AuthService */], __WEBPACK_IMPORTED_MODULE_20__guards_auth_guard__["a" /* AuthGuard */], __WEBPACK_IMPORTED_MODULE_18__services_data_service__["a" /* DataService */]],
            bootstrap: [__WEBPACK_IMPORTED_MODULE_5__app_component__["a" /* AppComponent */]]
        })
    ], AppModule);
    return AppModule;
}());



/***/ }),

/***/ "../../../../../src/app/components/categories/categories.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/components/categories/categories.component.html":
/***/ (function(module, exports) {

module.exports = "<div *ngFor=\"let category of categories\" style=\"padding-top: 5px; padding-bottom: 5px\" id=\"{{ category._id }}\">\n  <div class=\"card\">\n    <div class=\"card-header\">\n      <h5 class=\"card-title\">{{ category.name }}</h5>\n    </div>\n    <div class=\"card-body\">\n      <div class=\"row\">\n        <div class=\"col-sm-12\">\n          <p class=\"card-text\">Created by: {{ category.creator }}</p>\n          <button (click)=\"setAsCurrentCategory(category)\" class=\"btn btn-primary\">Set as current category</button>\n        </div>\n      </div>\n    </div>\n  </div>\n</div>\n<div class=\"card\">\n  <div class=\"card-header\">\n    <h5 class=\"card-title\">Add new category:</h5>\n  </div>\n  <div class=\"card-body\">\n    <div class=\"row\">\n      <div class=\"col-sm-12\">\n        <form (submit)=\"addNewCategory()\">\n          <div class=\"form-group\">\n            <input type=\"text\" [(ngModel)]=\"newCategoryTitle\" name=\"newCategoryTitle\" class=\"form-control\" id=\"newCategoryTitle\" placeholder=\"Enter new category\">\n          </div>\n          <button type=\"submit\" class=\"btn btn-primary\">Add category</button>\n        </form>\n      </div>\n    </div>\n  </div>\n</div>\n"

/***/ }),

/***/ "../../../../../src/app/components/categories/categories.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CategoriesComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__services_data_service__ = __webpack_require__("../../../../../src/app/services/data.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var CategoriesComponent = (function () {
    function CategoriesComponent(dataService) {
        this.dataService = dataService;
    }
    CategoriesComponent.prototype.ngOnInit = function () {
        this.listAllCategories();
    };
    CategoriesComponent.prototype.listAllCategories = function () {
        var _this = this;
        this.dataService.getAllCategories().subscribe(function (cats) {
            _this.categories = cats.cats;
            console.log(cats.cats);
        });
    };
    CategoriesComponent.prototype.addNewCategory = function () {
        var _this = this;
        console.log(this.newCategoryTitle);
        this.dataService.addNewCategory(this.newCategoryTitle).subscribe(function (res) {
            if (res.success) {
                _this.newCategoryTitle = "";
                _this.listAllCategories();
            }
            else {
                console.log("Entry could not be added");
            }
        });
    };
    CategoriesComponent.prototype.setAsCurrentCategory = function (category) {
        this.dataService.setCurrentCategory(category);
    };
    CategoriesComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-categories',
            template: __webpack_require__("../../../../../src/app/components/categories/categories.component.html"),
            styles: [__webpack_require__("../../../../../src/app/components/categories/categories.component.css")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__services_data_service__["a" /* DataService */]])
    ], CategoriesComponent);
    return CategoriesComponent;
}());



/***/ }),

/***/ "../../../../../src/app/components/dashboard/dashboard.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/components/dashboard/dashboard.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"col-sm-12\" style=\"hight: 100%;\">\n  <div class=\"card\">\n    <div class=\"card-header\">\n      <h5 class=\"card-title\">Data Options</h5>\n    </div>\n    <div class=\"card-body\">\n      <div class=\"btn-group\" role=\"group\" style=\"text-align:center; width: 100%;\">\n        <button type=\"button\" (click)=\"exportDisplayedData()\" class=\"btn btn-primary btn-sm\" style=\"text-align:center; width: 100%;\">Export displayed data</button>\n        <button type=\"button\" (click)=\"getExportedEntries()\" class=\"btn btn-secondary btn-sm\" style=\"text-align:center; width: 100%;\">Show exported data</button>\n        <button type=\"button\" (click)=\"getNotExportedEntries()\" class=\"btn btn-primary btn-sm\" style=\"text-align:center; width: 100%;\">Show not exported data</button>\n      </div>\n    </div>\n  </div>\n  <div class=\"card\">\n    <div class=\"card-body\">\n      <div style=\"overflow: auto\">\n        <table class=\"table\" id=\"data_table\">\n          <thead>\n            <tr>\n              <th scope=\"col\">News Type</th>\n              <th scope=\"col\">title</th>\n              <th scope=\"col\">date</th>\n              <th scope=\"col\">author</th>\n              <th scope=\"col\">publisher</th>\n              <th scope=\"col\">abstract</th>\n              <th scope=\"col\">content</th>\n              <th scope=\"col\">url</th>\n              <th scope=\"col\">application_field</th>\n              <th scope=\"col\">application</th>\n              <th scope=\"col\">user</th>\n              <th scope=\"col\">industry_user</th>\n              <th scope=\"col\">geography_user</th>\n              <th scope=\"col\">function</th>\n              <th scope=\"col\">application</th>\n              <th scope=\"col\">technology</th>\n              <th scope=\"col\">maturity_level</th>\n              <th scope=\"col\">technology_provider</th>\n              <th scope=\"col\">industry_provider</th>\n              <th scope=\"col\">geography_provider</th>\n              <th scope=\"col\">ecosystem</th>\n              <th scope=\"col\">application_trend</th>\n              <th scope=\"col\">technology_trend</th>\n              <th scope=\"col\">ecosystem_trend</th>\n              <th scope=\"col\">picked by user</th>\n            </tr>\n          </thead>\n          <tbody>\n            <tr *ngFor=\"let entry of collected\">\n              <td>Website</td>\n              <td>{{ entry?.title }}</td>\n              <td>{{ entry?.date }}</td>\n              <td>{{ entry?.author }}</td>\n              <td>{{ entry?.publisher }}</td>\n              <td>{{ entry?.abstract }}</td>\n              <td>{{ entry?.content }}</td>\n              <td>{{ entry?.url }}</td>\n              <td>{{ entry?.application_field }}</td>\n              <td>{{ entry?.application }}</td>\n              <td>{{ entry?.user }}</td>\n              <td>{{ entry?.industry_user }}</td>\n              <td>{{ entry?.geography_user }}</td>\n              <td>{{ entry?.function }}</td>\n              <td>{{ entry?.application }}</td>\n              <td>{{ entry?.technology }}</td>\n              <td>{{ entry?.maturity_level }}</td>\n              <td>{{ entry?.technology_provider }}</td>\n              <td>{{ entry?.industry_provider }}</td>\n              <td>{{ entry?.geography_provider }}</td>\n              <td>{{ entry?.ecosystem }}</td>\n              <td>{{ entry?.application_trend }}</td>\n              <td>{{ entry?.technology_trend }}</td>\n              <td>{{ entry?.ecosystem_trend }}</td>\n              <td>{{ entry?.picked_by }}</td>\n            </tr>\n          </tbody>\n        </table>\n      </div>\n    </div>\n  </div>\n</div>\n"

/***/ }),

/***/ "../../../../../src/app/components/dashboard/dashboard.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DashboardComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__services_data_service__ = __webpack_require__("../../../../../src/app/services/data.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_router__ = __webpack_require__("../../../router/esm5/router.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var DashboardComponent = (function () {
    function DashboardComponent(dataService, router) {
        this.dataService = dataService;
        this.router = router;
    }
    DashboardComponent.prototype.ngOnInit = function () {
    };
    DashboardComponent.prototype.getExportedEntries = function () {
        this.getCollectedEntries(true);
    };
    DashboardComponent.prototype.getNotExportedEntries = function () {
        this.getCollectedEntries(false);
    };
    DashboardComponent.prototype.getCollectedEntries = function (exported) {
        var _this = this;
        this.collected = "";
        this.dataService.getCollectedEntries(exported).subscribe(function (collected) {
            _this.collected = collected.collected;
            console.log(collected);
        });
    };
    DashboardComponent.prototype.exportDisplayedData = function () {
        var exportable = this.enrichDataObjectWithEmptyFields();
        this.exportToCSV(exportable);
    };
    DashboardComponent.prototype.enrichDataObjectWithEmptyFields = function () {
        var sourceData = this.collected;
        var JSONobject = [];
        var numberOfEntries = this.collected.length;
        for (var i = 0; i < numberOfEntries; i++) {
            var entryObject = this.dbObjectToCompleteObject(sourceData[i]);
            JSONobject.push(entryObject);
        }
        return JSONobject;
    };
    DashboardComponent.prototype.dbObjectToCompleteObject = function (dataObject) {
        var newObject = {};
        newObject.title = dataObject.title || "";
        newObject.date = dataObject.date || "";
        newObject.author = dataObject.author || "";
        newObject.publisher = dataObject.publisher || "";
        newObject.abstract = dataObject.abstract || "";
        newObject.content = dataObject.content || "";
        newObject.url = dataObject.url || "";
        newObject.application_field = dataObject.application_field || "";
        newObject.application = dataObject.application || "";
        newObject.user = dataObject.user || "";
        newObject.industry_user = dataObject.industry_user || "";
        newObject.geography_user = dataObject.geography_user || "";
        newObject.function = dataObject.function || "";
        newObject.application = dataObject.application || "";
        newObject.technology = dataObject.technology || "";
        newObject.maturity_level = dataObject.maturity_level || "";
        newObject.technology_provider = dataObject.technology_provider || "";
        newObject.geography_provider = dataObject.geography_provider || "";
        newObject.ecosystem = dataObject.ecosystem || "";
        newObject.application_trend = dataObject.application_trend || "";
        newObject.technology_trend = dataObject.technology_trend || "";
        newObject.ecosystem_trend = dataObject.ecosystem_trend || "";
        return newObject;
    };
    // https://stackoverflow.com/questions/18848860/javascript-array-to-csv/18849208#18849208
    DashboardComponent.prototype.objectToCSVRow = function (dataObject) {
        var dataArray = new Array;
        for (var o in dataObject) {
            var innerValue = dataObject[o] === null ? '' : dataObject[o].toString();
            var result = innerValue.replace(/"/g, '""');
            result = '"' + result + '"';
            dataArray.push(result);
        }
        return dataArray.join(' ') + '\r\n';
    };
    // https://stackoverflow.com/questions/18848860/javascript-array-to-csv/18849208#18849208
    DashboardComponent.prototype.exportToCSV = function (arrayOfObjects) {
        if (!arrayOfObjects.length) {
            return;
        }
        var csvContent = "data:text/csv;charset=utf-8,";
        // headers
        csvContent += this.objectToCSVRow(Object.keys(arrayOfObjects[0]));
        for (var _i = 0, arrayOfObjects_1 = arrayOfObjects; _i < arrayOfObjects_1.length; _i++) {
            var item = arrayOfObjects_1[_i];
            csvContent += this.objectToCSVRow(item);
        }
        var fileName = "deepReadRSSexport_" + this.getDate() + ".csv";
        var encodedUri = encodeURI(csvContent);
        var link = document.createElement("a");
        link.setAttribute("href", encodedUri);
        link.setAttribute("download", fileName);
        document.body.appendChild(link); // Required for FF
        link.click();
        document.body.removeChild(link);
    };
    DashboardComponent.prototype.getDate = function () {
        var today = new Date();
        var d = today.getDate();
        var m = today.getMonth() + 1;
        var yyyy = today.getFullYear();
        var dd;
        var mm;
        if (d < 10) {
            dd = '0' + d;
        }
        else {
            dd = d;
        }
        if (m < 10) {
            mm = '0' + m;
        }
        else {
            mm = m;
        }
        return yyyy + mm + dd;
    };
    DashboardComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-dashboard',
            template: __webpack_require__("../../../../../src/app/components/dashboard/dashboard.component.html"),
            styles: [__webpack_require__("../../../../../src/app/components/dashboard/dashboard.component.css")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__services_data_service__["a" /* DataService */],
            __WEBPACK_IMPORTED_MODULE_2__angular_router__["a" /* Router */]])
    ], DashboardComponent);
    return DashboardComponent;
}());



/***/ }),

/***/ "../../../../../src/app/components/feed/feed.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/components/feed/feed.component.html":
/***/ (function(module, exports) {

module.exports = "\n<div class=\"row\" style=\"padding-top: 5px; padding-bottom: 5px\">\n  <div class=\"col-sm-4\">\n    <div class=\"card\">\n      <div class=\"card-header\">\n        <h5 class=\"card-title\">Current category</h5>\n      </div>\n      <div class=\"card-body\">\n        {{ this.dataService.currentCategory.name }}\n      </div>\n    </div>\n  </div>\n  <div class=\"col-sm-4\" style=\"hight: 100%;\">\n    <div class=\"card\">\n      <div class=\"card-header\">\n        <h5 class=\"card-title\">Options</h5>\n      </div>\n      <div class=\"card-body\">\n        <div class=\"btn-group\" role=\"group\" style=\"text-align:center; width: 100%;\">\n          <button type=\"button\" (click)=\"fillFeedForCurrentCategory()\" class=\"btn btn-primary btn-sm\" style=\"text-align:center; width: 100%;\">Show new entries</button>\n          <button type=\"button\" (click)=\"fillFeedForCurrentCategoryWithDismissedEntries()\" class=\"btn btn-secondary btn-sm\" style=\"text-align:center; width: 100%;\">Show dismissed entries</button>\n        </div>\n      </div>\n    </div>\n  </div>\n  <div class=\"col-sm-4\">\n    <div class=\"card\">\n      <div class=\"card-header\">\n        <h5 class=\"card-title\">Stats</h5>\n      </div>\n      <div class=\"card-body\">\n        Number of {{ entry_adjective }} entries: {{ rss?.length }}\n      </div>\n    </div>\n  </div>\n</div>\n\n<div *ngFor=\"let entry of rss\" style=\"padding-top: 5px; padding-bottom: 5px\" id=\"{{ entry._id }}\">\n  <div class=\"card\">\n    <div class=\"card-header\">\n      <h5 class=\"card-title\">{{ entry?.title }}</h5>\n    </div>\n    <div class=\"card-body\">\n      <ul class=\"list-group list-group-flush\">\n        <li class=\"list-group-item\">\n          <div class=\"row\">\n            <div class=\"col-sm-12\">\n              <p class=\"card-text\">{{ entry?.description }}</p>\n            </div>\n          </div>\n        </li>\n        <li class=\"list-group-item\">\n          <div class=\"row\">\n            <div class=\"col-sm-12\">\n              <a href=\"{{ entry?.link }}\" target=\"_blank\">{{ entry?.link }}</a>\n            </div>\n          </div>\n        </li>\n        <li class=\"list-group-item\">\n          <div class=\"row\">\n            <div class=\"col-sm-6\" *ngIf=\"showsNewEnries\">\n              <button type=\"button\" (click)=\"markAsDismissed(entry._id)\" class=\"btn btn-secondary btn-sm\" style=\"text-align:center; width: 100%;\">Dismiss entry</button>\n            </div>\n            <div class=\"col-sm-6\">\n              <div style=\"text-align:block;\">\n                <button type=\"button\" (click)=\"markAsRelevant(entry._id)\" class=\"btn btn-success btn-sm\" style=\"text-align:center; width: 100%;\">Mark as relevant</button>\n              </div>\n            </div>\n          </div>\n        </li>\n      </ul>\n    </div>\n  </div>\n</div>\n"

/***/ }),

/***/ "../../../../../src/app/components/feed/feed.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return FeedComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__services_data_service__ = __webpack_require__("../../../../../src/app/services/data.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_router__ = __webpack_require__("../../../router/esm5/router.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var FeedComponent = (function () {
    function FeedComponent(dataService, router) {
        this.dataService = dataService;
        this.router = router;
        this.entry_adjective = "";
        this.showsNewEnries = true;
    }
    FeedComponent.prototype.ngOnInit = function () {
        this.fillFeedForCurrentCategory();
    };
    FeedComponent.prototype.fillFeedForCurrentCategory = function () {
        var _this = this;
        this.showsNewEnries = true;
        this.entry_adjective = "new";
        this.rss = "";
        var cat = this.dataService.getCurrentCategory();
        this.dataService.getEntries(false, false, false, cat._id).subscribe(function (rss) {
            _this.rss = rss.rss; //take only array not entire object
            console.log(rss);
        });
    };
    FeedComponent.prototype.fillFeedForCurrentCategoryWithDismissedEntries = function () {
        var _this = this;
        this.showsNewEnries = false;
        this.entry_adjective = "dismissed";
        this.rss = "";
        var cat = this.dataService.getCurrentCategory();
        this.dataService.getEntries(true, false, false, cat._id).subscribe(function (rss) {
            _this.rss = rss.rss; //take only array not entire object
            console.log(rss);
        });
    };
    FeedComponent.prototype.markAsDismissed = function (entry_id) {
        var _this = this;
        this.dataService.markAsDismissed(entry_id).subscribe(function (res) {
            if (res.success) {
                _this.removeEntryCard(entry_id);
            }
            else {
                console.log("Entry could not be dismissed");
            }
        });
    };
    FeedComponent.prototype.markAsRelevant = function (entry_id) {
        var _this = this;
        this.dataService.markAsRelevant(entry_id).subscribe(function (res) {
            if (res.success) {
                _this.removeEntryCard(entry_id);
            }
            else {
                console.log("Entry could not be marked as relevant");
            }
        });
    };
    FeedComponent.prototype.removeEntryCard = function (entry_id) {
        document.getElementById(entry_id).remove();
    };
    FeedComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-feed',
            template: __webpack_require__("../../../../../src/app/components/feed/feed.component.html"),
            styles: [__webpack_require__("../../../../../src/app/components/feed/feed.component.css")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__services_data_service__["a" /* DataService */],
            __WEBPACK_IMPORTED_MODULE_2__angular_router__["a" /* Router */]])
    ], FeedComponent);
    return FeedComponent;
}());



/***/ }),

/***/ "../../../../../src/app/components/home/home.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/components/home/home.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"jumbotron text-center\">\n  <h1>Test App</h1>\n  <p class=\"lead\">Welcome</p>\n  <div>\n    <a class=\"btn btn-primary\" [routerLink]=\"['/register']\">Register</a> <a class=\"btn btn-default\" [routerLink]=\"['/login']\">Login</a>\n  </div>\n</div>\n\n<div class=\"row\">\n  <div class=\"col-md-4\">\n    <h3>Express Backend</h3>\n    <p>A rock solid Node.js/Express server using Mongoose to organize models and...</p>\n  </div>\n  <div class=\"col-md-4\">\n    <h3>Angular CLI</h3>\n    <p>...to generate components, services and more.</p>\n  </div>\n  <div class=\"col-md-4\">\n    <h3>JWT Tokens</h3>\n    <p>do stuff</p>\n  </div>\n</div>\n"

/***/ }),

/***/ "../../../../../src/app/components/home/home.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return HomeComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var HomeComponent = (function () {
    function HomeComponent() {
    }
    HomeComponent.prototype.ngOnInit = function () {
    };
    HomeComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-home',
            template: __webpack_require__("../../../../../src/app/components/home/home.component.html"),
            styles: [__webpack_require__("../../../../../src/app/components/home/home.component.css")]
        }),
        __metadata("design:paramtypes", [])
    ], HomeComponent);
    return HomeComponent;
}());



/***/ }),

/***/ "../../../../../src/app/components/login/login.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/components/login/login.component.html":
/***/ (function(module, exports) {

module.exports = "<h2 class=\"page-header\">Login</h2>\n<form (submit)=\"onLoginSubmit()\">\n  <div class=\"form-group\">\n    <label>Username</label>\n    <input type=\"text\" class=\"form-control\" [(ngModel)]=\"username\" name=\"username\">\n  </div>\n  <div class=\"form-group\">\n    <label>Password</label>\n    <input type=\"password\" class=\"form-control\" [(ngModel)]=\"password\" name=\"password\">\n  </div>\n  <input type=\"submit\" class=\"btn btn-primary\" value=\"Login\">\n</form>\n"

/***/ }),

/***/ "../../../../../src/app/components/login/login.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LoginComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__services_auth_service__ = __webpack_require__("../../../../../src/app/services/auth.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_router__ = __webpack_require__("../../../router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_angular2_flash_messages__ = __webpack_require__("../../../../angular2-flash-messages/module/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_angular2_flash_messages___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_angular2_flash_messages__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var LoginComponent = (function () {
    function LoginComponent(authService, router, flashMessage) {
        this.authService = authService;
        this.router = router;
        this.flashMessage = flashMessage;
    }
    LoginComponent.prototype.ngOnInit = function () {
    };
    LoginComponent.prototype.onLoginSubmit = function () {
        var _this = this;
        var user = {
            username: this.username,
            password: this.password
        };
        this.authService.authenticateUser(user).subscribe(function (data) {
            if (data.success) {
                _this.authService.storeUserData(data.token, data.user);
                _this.flashMessage.show('You are now logged in', { cssClass: 'alert-success', timeout: 5000 });
                _this.router.navigate(['dashboard']);
            }
            else {
                _this.flashMessage.show(data.msg, { cssClass: 'alert-danger', timeout: 5000 });
                _this.router.navigate(['login']);
            }
        });
    };
    LoginComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-login',
            template: __webpack_require__("../../../../../src/app/components/login/login.component.html"),
            styles: [__webpack_require__("../../../../../src/app/components/login/login.component.css")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__services_auth_service__["a" /* AuthService */],
            __WEBPACK_IMPORTED_MODULE_2__angular_router__["a" /* Router */],
            __WEBPACK_IMPORTED_MODULE_3_angular2_flash_messages__["FlashMessagesService"]])
    ], LoginComponent);
    return LoginComponent;
}());



/***/ }),

/***/ "../../../../../src/app/components/navbar/navbar.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/components/navbar/navbar.component.html":
/***/ (function(module, exports) {

module.exports = "<nav class=\"navbar navbar-expand-lg navbar-dark bg-primary\">\n  <a class=\"navbar-brand\" href=\"#\">Deep ReadRRS</a>\n  <button class=\"navbar-toggler\" type=\"button\" data-toggle=\"collapse\" data-target=\"#navbarColor01\" aria-controls=\"navbarColor01\" aria-expanded=\"false\" aria-label=\"Toggle navigation\" style=\"\">\n    <span class=\"navbar-toggler-icon\"></span>\n  </button>\n<!--\n  <div class=\"collapse navbar-collapse\" id=\"navbarColor01\">\n    <ul class=\"navbar-nav mr-auto\">\n      <li class=\"nav-item active\">\n        <a class=\"nav-link\" href=\"#\">Home <span class=\"sr-only\">(current)</span></a>\n      </li>\n      <li class=\"nav-item\">\n        <a class=\"nav-link\" href=\"#\">Features</a>\n      </li>\n      <li class=\"nav-item\">\n        <a class=\"nav-link\" href=\"#\">Pricing</a>\n      </li>\n      <li class=\"nav-item\">\n        <a class=\"nav-link\" href=\"#\">About</a>\n      </li>\n    </ul>\n  -->\n    <!--\n    <form class=\"form-inline my-2 my-lg-0\">\n      <input class=\"form-control mr-sm-2\" type=\"text\" placeholder=\"Search\">\n      <button class=\"btn btn-secondary my-2 my-sm-0\" type=\"submit\">Search</button>\n    </form>\n\n  </div>\n-->\n  <div class=\"collapse navbar-collapse\" id=\"navbarColor01\">\n    <ul class=\"navbar-nav mr-auto\">\n      <li class=\"nav-item\" [routerLinkActive]=\"['active']\" [routerLinkActiveOptions]=\"{exact:true}\"><a class=\"nav-link\" [routerLink]=\"['/']\">Home</a></li>\n      <li class=\"nav-item\" *ngIf=\"authService.loggedIn()\" [routerLinkActive]=\"['active']\" [routerLinkActiveOptions]=\"{exact:true}\"><a class=\"nav-link\" [routerLink]=\"['/dashboard']\">Dashboard</a></li>\n      <li class=\"nav-item\" *ngIf=\"authService.loggedIn()\" [routerLinkActive]=\"['active']\" [routerLinkActiveOptions]=\"{exact:true}\"><a class=\"nav-link\" [routerLink]=\"['/feed']\">Feed</a></li>\n      <li class=\"nav-item\" *ngIf=\"authService.loggedIn()\" [routerLinkActive]=\"['active']\" [routerLinkActiveOptions]=\"{exact:true}\"><a class=\"nav-link\" [routerLink]=\"['/picker']\">Picker</a></li>\n      <li class=\"nav-item\" *ngIf=\"authService.loggedIn()\" [routerLinkActive]=\"['active']\" [routerLinkActiveOptions]=\"{exact:true}\"><a class=\"nav-link\" [routerLink]=\"['/categories']\">Categories</a></li>\n      <li class=\"nav-item\" *ngIf=\"authService.loggedIn()\" [routerLinkActive]=\"['active']\" [routerLinkActiveOptions]=\"{exact:true}\"><a class=\"nav-link\" [routerLink]=\"['/sources']\">Sources</a></li>\n      <li class=\"nav-item\" *ngIf=\"authService.loggedIn()\" [routerLinkActive]=\"['active']\" [routerLinkActiveOptions]=\"{exact:true}\"><a class=\"nav-link\" class=\"nav-link\" [routerLink]=\"['/profile']\">Profile</a></li>\n      <li class=\"nav-item\" *ngIf=\"!authService.loggedIn()\" [routerLinkActive]=\"['active']\" [routerLinkActiveOptions]=\"{exact:true}\"><a class=\"nav-link\" [routerLink]=\"['/login']\">Login</a></li>\n      <li class=\"nav-item\" *ngIf=\"!authService.loggedIn()\" [routerLinkActive]=\"['active']\" [routerLinkActiveOptions]=\"{exact:true}\"><a class=\"nav-link\" [routerLink]=\"['/register']\">Register</a></li>\n      <li class=\"nav-item\" *ngIf=\"authService.loggedIn()\"><a class=\"nav-link\" (click)=\"onLogoutClick()\" href=\"#\">Logout</a></li>\n    </ul>\n  </div>\n</nav>\n\n<!--\n<nav class=\"navbar navbar-default\">\n  <div class=\"container\">\n    <div class=\"navbar-header\">\n      <button type=\"button\" class=\"navbar-toggle collapsed\" data-toggle=\"collapse\" data-target=\"#navbar\" aria-expanded=\"false\" aria-controls=\"navbar\">\n        <span class=\"sr-only\">Toggle navigation</span>\n        <span class=\"icon-bar\"></span>\n        <span class=\"icon-bar\"></span>\n        <span class=\"icon-bar\"></span>\n      </button>\n      <a class=\"navbar-brand\" href=\"#\">MEAN app</a>\n    </div>\n    <div id=\"navbar\" class=\"collapse navbar-collapse\">\n      <ul class=\"nav navbar-nav navbar-left\">\n        <li [routerLinkActive]=\"['active']\" [routerLinkActiveOptions]=\"{exact:true}\"><a [routerLink]=\"['/']\">Home</a></li>\n      </ul>\n      <ul class=\"nav navbar-nav navbar-right\">\n        <li *ngIf=\"authService.loggedIn()\" [routerLinkActive]=\"['active']\" [routerLinkActiveOptions]=\"{exact:true}\"><a [routerLink]=\"['/dashboard']\">Dashboard</a></li>\n        <li *ngIf=\"authService.loggedIn()\" [routerLinkActive]=\"['active']\" [routerLinkActiveOptions]=\"{exact:true}\"><a [routerLink]=\"['/feed']\">Feed</a></li>\n        <li *ngIf=\"authService.loggedIn()\" [routerLinkActive]=\"['active']\" [routerLinkActiveOptions]=\"{exact:true}\"><a [routerLink]=\"['/picker']\">Picker</a></li>\n        <li *ngIf=\"authService.loggedIn()\" [routerLinkActive]=\"['active']\" [routerLinkActiveOptions]=\"{exact:true}\"><a [routerLink]=\"['/profile']\">Profile</a></li>\n        <li *ngIf=\"!authService.loggedIn()\" [routerLinkActive]=\"['active']\" [routerLinkActiveOptions]=\"{exact:true}\"><a [routerLink]=\"['/login']\">Login</a></li>\n        <li *ngIf=\"!authService.loggedIn()\" [routerLinkActive]=\"['active']\" [routerLinkActiveOptions]=\"{exact:true}\"><a [routerLink]=\"['/register']\">Register</a></li>\n        <li *ngIf=\"authService.loggedIn()\"><a (click)=\"onLogoutClick()\" href=\"#\">Logout</a></li>\n      </ul>\n    </div>\n  --><!--/.nav-collapse --><!--\n  </div>\n</nav>\n-->\n"

/***/ }),

/***/ "../../../../../src/app/components/navbar/navbar.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return NavbarComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__services_auth_service__ = __webpack_require__("../../../../../src/app/services/auth.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_router__ = __webpack_require__("../../../router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_angular2_flash_messages__ = __webpack_require__("../../../../angular2-flash-messages/module/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_angular2_flash_messages___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_angular2_flash_messages__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var NavbarComponent = (function () {
    function NavbarComponent(authService, router, flashMessage) {
        this.authService = authService;
        this.router = router;
        this.flashMessage = flashMessage;
    }
    NavbarComponent.prototype.ngOnInit = function () {
    };
    NavbarComponent.prototype.onLogoutClick = function () {
        this.authService.logout();
        this.flashMessage.show('You are logged out', { cssClass: 'alert-success', timeout: 3000 });
        this.router.navigate(['/login']);
        return false;
    };
    NavbarComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-navbar',
            template: __webpack_require__("../../../../../src/app/components/navbar/navbar.component.html"),
            styles: [__webpack_require__("../../../../../src/app/components/navbar/navbar.component.css")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__services_auth_service__["a" /* AuthService */],
            __WEBPACK_IMPORTED_MODULE_2__angular_router__["a" /* Router */],
            __WEBPACK_IMPORTED_MODULE_3_angular2_flash_messages__["FlashMessagesService"]])
    ], NavbarComponent);
    return NavbarComponent;
}());



/***/ }),

/***/ "../../../../../src/app/components/picker/picker.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "#bootstrap-overrides iframe {\r\n  border: 20;\r\n  border-color: #333333;\r\n}\r\n\r\n.form-group {\r\n    padding-top: 0px;\r\n    padding-right: 0px;\r\n    padding-bottom: 0px;\r\n    padding-left: 0px;\r\n}\r\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/components/picker/picker.component.html":
/***/ (function(module, exports) {

module.exports = "<div id=\"bootstrap-overrides\">\n  <div class=\"row\">\n    <div class=\"col-md-2\">\n      <div *ngFor=\"let entry of rss\" style=\"padding-top: 5px; padding-bottom: 5px\" id=\"{{ entry._id }}\">\n        <div class=\"card\">\n          <div class=\"card-header\"  (click)=\"setCurrentEntry(entry)\">\n            <h5 class=\"card-title\">{{ entry.title }}</h5>\n          </div>\n          <button class=\"btn secondary btn-sm\" (click)=\"markAsDismissed(entry._id)\" type=\"button\">\n            Mark as dismissed\n          </button>\n        </div>\n      </div>\n\n    </div>\n    <div class=\"col-md-8\">\n      <a class=\"btn btn-secondary btn-sm\" id=\"mybutton\" href=\"\" target=\"_blank\" role=\"button\" style=\"text-align:center; width: 100%; padding-top: 5px; padding-bottom: 5px\">Open link in new tab</a>\n      <div class=\"embed-responsive embed-responsive-1by1\">\n        <div class=\"solidborder\">\n          <iframe id=\"myiframe\" sandbox=\"allow-forms\" class=\"embed-responsive-item\" src=\"\"></iframe>\n        </div>\n      </div>\n    </div>\n    <div class=\"col-md-2\">\n      <div style=\"padding-top: 5px; padding-bottom: 5px\">\n        <div class=\"card\">\n          <div class=\"card-header\">\n            Fill form:\n          </div>\n          <div class=\"card-body\">\n            <form (submit)=\"submitCollectedEntry()\">\n              <div class=\"form-group form-control-sm\">\n                <label for=\"title\">Title</label>\n                <input type=\"text\" [(ngModel)]=\"title\" name=\"title\" class=\"form-control form-control-sm\" id=\"title\" placeholder=\"Title\" required>\n              </div>\n              <div class=\"form-group form-control-sm\">\n                <label for=\"date\">Date</label>\n                <input type=\"date\" [(ngModel)]=\"date\" name=\"date\" class=\"form-control form-control-sm\" id=\"date\" placeholder=\"DD.MM.YYYY\" required>\n              </div>\n              <div class=\"form-group form-control-sm\">\n                <label for=\"author\">Author</label>\n                <input type=\"text\" [(ngModel)]=\"author\" name=\"author\" class=\"form-control form-control-sm\" id=\"author\" placeholder=\"Author\">\n              </div>\n              <div class=\"form-group form-control-sm\">\n                <label for=\"publisher\">Publisher</label>\n                <input type=\"text\" [(ngModel)]=\"publisher\" name=\"publisher\" class=\"form-control form-control-sm\" id=\"publisher\" placeholder=\"Publisher\">\n              </div>\n              <div class=\"form-group form-control-sm\">\n                <label for=\"abstract\">Abstract</label>\n                <textarea type=\"text\" [(ngModel)]=\"abstract\" name=\"abstract\" class=\"form-control form-control-sm\" id=\"abstract\" placeholder=\"Abstract\" rows=\"3\"></textarea>\n              </div>\n              <div class=\"form-group form-control-sm\">\n                <label for=\"content\">Content</label>\n                <textarea type=\"text\" [(ngModel)]=\"content\" name=\"content\" class=\"form-control form-control-sm\" id=\"content\" placeholder=\"Content\" rows=\"3\"></textarea>\n              </div>\n              <div class=\"form-group form-control-sm\">\n                <label for=\"url\">URL</label>\n                <input type=\"url\" [(ngModel)]=\"url\" name=\"url\" class=\"form-control form-control-sm\" id=\"url\" placeholder=\"URL\" required>\n              </div>\n              <div class=\"form-group form-control-sm\">\n                <label for=\"application_field\">Application field</label>\n                <input type=\"application_field\" [(ngModel)]=\"application_field\" name=\"application_field\" class=\"form-control form-control-sm\" id=\"application_field\" placeholder=\"Application field\">\n              </div>\n              <div class=\"form-group form-control-sm\">\n                <label for=\"application\">Application</label>\n                <input type=\"application\" [(ngModel)]=\"form_application\" name=\"form_application\" class=\"form-control form-control-sm\" id=\"application\" placeholder=\"Application\">\n              </div>\n              <div class=\"form-group form-control-sm\">\n                <label for=\"user\">User</label>\n                <input type=\"text\" [(ngModel)]=\"user\" name=\"user\" class=\"form-control form-control-sm\" id=\"user\" placeholder=\"User\">\n              </div>\n              <div class=\"form-group form-control-sm\">\n                <label for=\"industry_user\">Industry (user)</label>\n                <input type=\"text\" [(ngModel)]=\"industry_user\" name=\"industry_user\" class=\"form-control form-control-sm\" id=\"industry_user\" placeholder=\"Industry (user)\">\n              </div>\n              <div class=\"form-group form-control-sm\">\n                <label for=\"geography_user\">Geography (user)</label>\n                <input type=\"text\" [(ngModel)]=\"geography_user\" name=\"geography_user\" class=\"form-control form-control-sm\" id=\"geography_user\" placeholder=\"Geography (user)\">\n              </div>\n              <div class=\"form-group form-control-sm\">\n                <label for=\"function\">Function</label>\n                <input type=\"text\" [(ngModel)]=\"function\" name=\"function\" class=\"form-control form-control-sm\" id=\"function\" placeholder=\"Function\">\n              </div>\n              <div class=\"form-group form-control-sm\">\n                <label for=\"technology\">Technology</label>\n                <input type=\"text\" [(ngModel)]=\"technology\" name=\"technology\" class=\"form-control form-control-sm\" id=\"technology\" placeholder=\"Technology\">\n              </div>\n              <div class=\"form-group form-control-sm\">\n                <label for=\"maturity_level\">Maturity Level</label>\n                <input type=\"text\" [(ngModel)]=\"maturity_level\" name=\"maturity_level\" class=\"form-control form-control-sm\" id=\"maturity_level\" placeholder=\"Maturity Level\">\n              </div>\n              <div class=\"form-group form-control-sm\">\n                <label for=\"technology_provider\">Technology provider</label>\n                <input type=\"text\" [(ngModel)]=\"technology_provider\" name=\"technology_provider\" class=\"form-control form-control-sm\" id=\"technology_provider\" placeholder=\"Technology provider\">\n              </div>\n              <div class=\"form-group form-control-sm\">\n                <label for=\"industry_provider\">Industry (provider)</label>\n                <input type=\"text\" [(ngModel)]=\"industry_provider\" name=\"industry_provider\" class=\"form-control form-control-sm\" id=\"industry_provider\" placeholder=\"Industry (provider)\">\n              </div>\n              <div class=\"form-group form-control-sm\">\n                <label for=\"geography_provider\">Geography (provider)</label>\n                <input type=\"text\" [(ngModel)]=\"geography_provider\" name=\"geography_provider\" class=\"form-control form-control-sm\" id=\"geography_provider\" placeholder=\"Geography (provider)\">\n              </div>\n              <div class=\"form-group form-control-sm\">\n                <label for=\"ecosystem\">Ecosystem</label>\n                <input type=\"text\" [(ngModel)]=\"ecosystem\" name=\"ecosystem\" class=\"form-control form-control-sm\" id=\"ecosystem\" placeholder=\"Ecosystem\">\n              </div>\n              <div class=\"form-group form-control-sm\">\n                <label for=\"application_trend\">Application trend</label>\n                <input type=\"text\" [(ngModel)]=\"application_trend\" name=\"application_trend\" class=\"form-control form-control-sm\" id=\"application_trend\" placeholder=\"Application trend\">\n              </div>\n              <div class=\"form-group form-control-sm\">\n                <label for=\"technology_trend\">Technology trend</label>\n                <input type=\"text\" [(ngModel)]=\"technology_trend\" name=\"technology_trend\" class=\"form-control form-control-sm\" id=\"technology_trend\" placeholder=\"Technology trend\">\n              </div>\n              <div class=\"form-group form-control-sm\">\n                <label for=\"ecosystem_trend\">Ecosystem Trend</label>\n                <input type=\"text\" [(ngModel)]=\"ecosystem_trend\" name=\"ecosystem_trend\" class=\"form-control form-control-sm\" id=\"ecosystem_trend\" placeholder=\"Ecosystem Trend\">\n              </div>\n              <button type=\"submit\" class=\"btn btn-primary\">Submit</button>\n            </form>\n          </div>\n        </div>\n      </div>\n    </div>\n  </div>\n</div>\n"

/***/ }),

/***/ "../../../../../src/app/components/picker/picker.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PickerComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__services_data_service__ = __webpack_require__("../../../../../src/app/services/data.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_angular2_flash_messages__ = __webpack_require__("../../../../angular2-flash-messages/module/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_angular2_flash_messages___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_angular2_flash_messages__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var PickerComponent = (function () {
    function PickerComponent(dataService, _flashMessagesService) {
        this.dataService = dataService;
        this._flashMessagesService = _flashMessagesService;
    }
    PickerComponent.prototype.ngOnInit = function () {
        this.setUpPicker();
    };
    PickerComponent.prototype.setUpPicker = function () {
        var _this = this;
        this.dataService.getEntries(false, true, false, this.dataService.getCurrentCategory()._id).subscribe(function (rss) {
            _this.rss = rss.rss; //take only array not entire object
            _this.setCurrentEntry(_this.rss[0]);
            console.log(rss);
            console.log(_this.currentEntry);
        });
    };
    PickerComponent.prototype.setCurrentEntry = function (entry) {
        localStorage.setItem("current_entry", entry);
        this.currentEntry = entry;
        document.getElementById("myiframe").setAttribute("src", entry.link);
        document.getElementById("mybutton").setAttribute("href", entry.link);
        this.title = entry.title;
        this.url = entry.link;
        this.abstract = entry.summary;
        this.content = entry.summary;
    };
    PickerComponent.prototype.submitCollectedEntry = function () {
        var _this = this;
        var collected_data = {
            title: this.title,
            date: this.date,
            author: this.author,
            publisher: this.publisher,
            abstact: this.abstract,
            content: this.content,
            url: this.url,
            application_field: this.application_field,
            application: this.application,
            user: this.user,
            industry_user: this.industry_user,
            geography_user: this.geography_user,
            function: this.function,
            form_application: this.application,
            technology: this.technology,
            maturity_level: this.maturity_level,
            technology_provider: this.technology_provider,
            industry_provider: this.industry_provider,
            geography_provider: this.geography_provider,
            ecosystem: this.ecosystem,
            application_trend: this.application_trend,
            technology_trend: this.technology_trend,
            ecosystem_trend: this.ecosystem_trend
        };
        console.log("this entry should be submitted: " + this.currentEntry._id);
        this.dataService.addEntryToCollected(this.currentEntry._id, collected_data).subscribe(function (data) {
            if (data.success) {
                _this._flashMessagesService.show('Data saved successfully', { cssClass: 'alert-success', timeout: 8000 });
                _this.setUpPicker();
            }
            else {
                console.log("entry could not be added");
                _this._flashMessagesService.show('Something went wrong', { cssClass: 'alert-danger', timeout: 8000 });
            }
        });
    };
    //same as in feed.component.ts
    PickerComponent.prototype.markAsDismissed = function (entry_id) {
        var _this = this;
        this.dataService.markAsDismissed(entry_id).subscribe(function (res) {
            if (res.success) {
                _this.removeEntryCard(entry_id);
            }
            else {
                console.log("Entry could not be dismissed");
            }
        });
    };
    PickerComponent.prototype.removeEntryCard = function (entry_id) {
        document.getElementById(entry_id).remove();
        this.setUpPicker();
    };
    ;
    PickerComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-picker',
            template: __webpack_require__("../../../../../src/app/components/picker/picker.component.html"),
            styles: [__webpack_require__("../../../../../src/app/components/picker/picker.component.css")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__services_data_service__["a" /* DataService */],
            __WEBPACK_IMPORTED_MODULE_2_angular2_flash_messages__["FlashMessagesService"]])
    ], PickerComponent);
    return PickerComponent;
}());



/***/ }),

/***/ "../../../../../src/app/components/profile/profile.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/components/profile/profile.component.html":
/***/ (function(module, exports) {

module.exports = "<div *ngIf=\"user\">\n  <h2 class=\"page-header\">{{user.name}}</h2>\n  <ul class=\"list-group\">\n    <li class=\"list-group-item\">Username: {{user.username}}</li>\n    <li class=\"list-group-item\">Email: {{user.email}}</li>\n  </ul>\n<div>\n\n<div *ngIf=\"usersToEnable\">\n  <div class=\"card\">\n    <div class=\"card-header\">\n      <h5 class=\"card-title\">Not enabled users:</h5>\n    </div>\n    <div class=\"card-body\">\n      <ul class=\"list-group list-group-flush\">\n        <li class=\"list-group-item\" style=\"padding-top: 5px; padding-bottom: 5px\">\n          <div class=\"row\">\n            <div class=\"col-sm-2\">\n              <p class=\"card-text\">Name</p>\n            </div>\n            <div class=\"col-sm-2\">\n              <p class=\"card-text\">Email</p>\n            </div>\n            <div class=\"col-sm-2\">\n              <p class=\"card-text\">Username</p>\n            </div>\n            <div class=\"col-sm-2\">\n              <p class=\"card-text\"></p>\n            </div>\n            <div class=\"col-sm-2\">\n              <p class=\"card-text\"></p>\n            </div>\n            <div class=\"col-sm-2\">\n              <p class=\"card-text\"></p>\n            </div>\n          </div>\n        </li>\n        <li class=\"list-group-item\" *ngFor=\"let user of notEnabledUsers\" style=\"padding-top: 5px; padding-bottom: 5px\" id=\"{{ user._id }}\">\n          <div class=\"row\">\n            <div class=\"col-sm-2\">\n              <p class=\"card-text\">{{ user.name }}</p>\n            </div>\n            <div class=\"col-sm-2\">\n              <p class=\"card-text\">{{ user.email }}</p>\n            </div>\n            <div class=\"col-sm-2\">\n              <p class=\"card-text\">{{ user.username }}</p>\n            </div>\n            <div class=\"col-sm-2\">\n              <p class=\"card-text\"></p>\n            </div>\n            <div class=\"col-sm-2\">\n              <p class=\"card-text\"><button type=\"button\" (click)=\"enableUser(user._id)\" class=\"btn btn-success btn-sm\" style=\"text-align:center; width: 100%;\">Authorize</button></p>\n            </div>\n            <div class=\"col-sm-2\">\n              <p class=\"card-text\"><button type=\"button\" (click)=\"deleteUser(user._id)\" class=\"btn btn-danger btn-sm\" style=\"text-align:center; width: 100%;\">Delete</button></p>\n            </div>\n          </div>\n        </li>\n      </ul>\n    </div>\n  </div>\n<div>\n"

/***/ }),

/***/ "../../../../../src/app/components/profile/profile.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ProfileComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__services_auth_service__ = __webpack_require__("../../../../../src/app/services/auth.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_router__ = __webpack_require__("../../../router/esm5/router.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var ProfileComponent = (function () {
    function ProfileComponent(authService, router) {
        this.authService = authService;
        this.router = router;
    }
    ProfileComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.usersToEnable = false;
        this.authService.getProfile().subscribe(function (profile) {
            _this.user = profile.user;
        }, function (err) {
            console.log(err);
            return false;
        });
        this.authService.getNotEnabledusers().subscribe(function (users) {
            if (users.success) {
                _this.notEnabledUsers = users.users;
                if (users.users.length > 0) {
                    _this.usersToEnable = true;
                }
            }
        });
    };
    ProfileComponent.prototype.enableUser = function (user_id) {
        var _this = this;
        console.log("try to enable user with id ", user_id);
        this.authService.enableUser(user_id).subscribe(function (res) {
            if (res.success) {
                console.log("user enabled");
                _this.ngOnInit();
            }
            else {
                console.log("user could not be enabled");
            }
        });
    };
    ProfileComponent.prototype.deleteUser = function (user_id) {
        var _this = this;
        console.log("try to delete user with id ", user_id);
        this.authService.deleteUser(user_id).subscribe(function (res) {
            if (res.success) {
                console.log("user deleted");
                _this.ngOnInit();
            }
            else {
                console.log("user could not be deleted");
            }
        });
    };
    ProfileComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-profile',
            template: __webpack_require__("../../../../../src/app/components/profile/profile.component.html"),
            styles: [__webpack_require__("../../../../../src/app/components/profile/profile.component.css")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__services_auth_service__["a" /* AuthService */],
            __WEBPACK_IMPORTED_MODULE_2__angular_router__["a" /* Router */]])
    ], ProfileComponent);
    return ProfileComponent;
}());



/***/ }),

/***/ "../../../../../src/app/components/register/register.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/components/register/register.component.html":
/***/ (function(module, exports) {

module.exports = "<h2 class=\"page-header\">Register</h2>\n<form (submit)=\"onRegisterSubmit()\">\n  <div class=\"form-group\">\n    <label>Name</label>\n    <input type=\"text\" [(ngModel)]=\"name\" name=\"name\" class=\"form-control\">\n  </div>\n  <div class=\"form-group\">\n    <label>Username</label>\n    <input type=\"text\" [(ngModel)]=\"username\" name=\"username\" class=\"form-control\">\n  </div>\n  <div class=\"form-group\">\n    <label>Email</label>\n    <input type=\"text\" [(ngModel)]=\"email\" name=\"email\" class=\"form-control\">\n  </div>\n  <div class=\"form-group\">\n    <label>Password</label>\n    <input type=\"password\" [(ngModel)]=\"password\" name=\"password\" class=\"form-control\">\n  </div>\n  <input type=\"submit\" class=\"btn btn-primary\" value=\"Submit\">\n</form>\n"

/***/ }),

/***/ "../../../../../src/app/components/register/register.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return RegisterComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__services_validate_service__ = __webpack_require__("../../../../../src/app/services/validate.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__services_auth_service__ = __webpack_require__("../../../../../src/app/services/auth.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_angular2_flash_messages__ = __webpack_require__("../../../../angular2-flash-messages/module/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_angular2_flash_messages___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_angular2_flash_messages__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__angular_router__ = __webpack_require__("../../../router/esm5/router.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var RegisterComponent = (function () {
    function RegisterComponent(validateService, _flashMessagesService, authService, router) {
        this.validateService = validateService;
        this._flashMessagesService = _flashMessagesService;
        this.authService = authService;
        this.router = router;
    }
    RegisterComponent.prototype.ngOnInit = function () {
    };
    RegisterComponent.prototype.onRegisterSubmit = function () {
        var _this = this;
        var user = {
            name: this.name,
            email: this.email,
            username: this.username,
            password: this.password
        };
        //Required Fields
        if (!this.validateService.validateRegister(user)) {
            this._flashMessagesService.show('Please fill in all fields', { cssClass: 'alert-danger', timeout: 8000 });
            return false;
        }
        //Validate Email
        if (!this.validateService.validateEmail(user.email)) {
            this._flashMessagesService.show('Please use valid email', { cssClass: 'alert-danger', timeout: 8000 });
            return false;
        }
        // Register user
        this.authService.registerUser(user).subscribe(function (data) {
            if (data.success) {
                _this._flashMessagesService.show('You are now registered. Ask your supervisor to authorize your account.', { cssClass: 'alert-success', timeout: 8000 });
                _this.router.navigate(['/login']);
            }
            else {
                _this._flashMessagesService.show('Something went wrong', { cssClass: 'alert-danger', timeout: 8000 });
                _this.router.navigate(['/register']);
            }
        });
    };
    RegisterComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-register',
            template: __webpack_require__("../../../../../src/app/components/register/register.component.html"),
            styles: [__webpack_require__("../../../../../src/app/components/register/register.component.css")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__services_validate_service__["a" /* ValidateService */],
            __WEBPACK_IMPORTED_MODULE_3_angular2_flash_messages__["FlashMessagesService"],
            __WEBPACK_IMPORTED_MODULE_2__services_auth_service__["a" /* AuthService */],
            __WEBPACK_IMPORTED_MODULE_4__angular_router__["a" /* Router */]])
    ], RegisterComponent);
    return RegisterComponent;
}());



/***/ }),

/***/ "../../../../../src/app/components/sources/sources.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/components/sources/sources.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"card\">\n  <div class=\"card-header\">\n    <h5 class=\"card-title\">Add new source to category {{ dataService.currentCategory.name }}:</h5>\n  </div>\n  <div class=\"card-body\">\n    <div class=\"row\">\n      <div class=\"col-sm-12\">\n        <form (submit)=\"addNewSource()\">\n          <div class=\"form-group\">\n            <input type=\"text\" [(ngModel)]=\"newSourceUrl\" name=\"newSourceUrl\" class=\"form-control\" id=\"newSourceUrl\" placeholder=\"Enter new source url\">\n          </div>\n          <button type=\"submit\" class=\"btn btn-primary\">Add source</button>\n        </form>\n      </div>\n    </div>\n  </div>\n</div>\n<div *ngFor=\"let source of sources\" style=\"padding-top: 5px; padding-bottom: 5px\" id=\"{{ source._id }}\">\n  <div class=\"card\">\n    <div class=\"card-header\">\n      <h5 class=\"card-title\">{{ source.url }}</h5>\n    </div>\n    <div class=\"card-body\">\n      <div class=\"row\">\n        <div class=\"col-sm-12\">\n          <p class=\"card-text\"></p>\n        </div>\n      </div>\n    </div>\n  </div>\n</div>\n"

/***/ }),

/***/ "../../../../../src/app/components/sources/sources.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SourcesComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__services_data_service__ = __webpack_require__("../../../../../src/app/services/data.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var SourcesComponent = (function () {
    function SourcesComponent(dataService) {
        this.dataService = dataService;
    }
    SourcesComponent.prototype.ngOnInit = function () {
        this.listAllSources();
    };
    //lists all sources of dataService.currentCategory
    SourcesComponent.prototype.listAllSources = function () {
        var _this = this;
        this.dataService.getAllSourcesOfCategory(this.dataService.currentCategory._id).subscribe(function (sources) {
            _this.sources = sources.sources;
        });
    };
    SourcesComponent.prototype.addNewSource = function () {
        var _this = this;
        console.log("Add new source in source component called with arguments: " + this.newSourceUrl + " and " + this.dataService.currentCategory._id);
        this.dataService.addNewSourcetoCategory(this.newSourceUrl, this.dataService.currentCategory._id).subscribe(function (res) {
            if (res.success) {
                _this.newSourceUrl = "";
                _this.listAllSources();
            }
            else {
                console.log("Entry could not be added");
            }
        });
    };
    SourcesComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-sources',
            template: __webpack_require__("../../../../../src/app/components/sources/sources.component.html"),
            styles: [__webpack_require__("../../../../../src/app/components/sources/sources.component.css")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__services_data_service__["a" /* DataService */]])
    ], SourcesComponent);
    return SourcesComponent;
}());



/***/ }),

/***/ "../../../../../src/app/guards/auth.guard.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AuthGuard; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("../../../router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__services_auth_service__ = __webpack_require__("../../../../../src/app/services/auth.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var AuthGuard = (function () {
    function AuthGuard(authService, router) {
        this.authService = authService;
        this.router = router;
    }
    AuthGuard.prototype.canActivate = function () {
        if (this.authService.loggedIn()) {
            return true;
        }
        else {
            this.router.navigate(['/login']);
            return false;
        }
    };
    AuthGuard = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2__services_auth_service__["a" /* AuthService */],
            __WEBPACK_IMPORTED_MODULE_1__angular_router__["a" /* Router */]])
    ], AuthGuard);
    return AuthGuard;
}());



/***/ }),

/***/ "../../../../../src/app/services/auth.service.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AuthService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_http__ = __webpack_require__("../../../http/esm5/http.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map__ = __webpack_require__("../../../../rxjs/_esm5/add/operator/map.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_angular2_jwt__ = __webpack_require__("../../../../angular2-jwt/angular2-jwt.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_angular2_jwt___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_angular2_jwt__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var AuthService = (function () {
    function AuthService(http) {
        this.http = http;
    }
    AuthService.prototype.registerUser = function (user) {
        var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["Headers"]();
        headers.append('Content-Type', 'application/json');
        return this.http.post('http://localhost:3000/users/register', user, { headers: headers })
            .map(function (res) { return res.json(); });
    };
    AuthService.prototype.authenticateUser = function (user) {
        var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["Headers"]();
        headers.append('Content-Type', 'application/json');
        return this.http.post('http://localhost:3000/users/authenticate', user, { headers: headers })
            .map(function (res) { return res.json(); });
    };
    AuthService.prototype.getProfile = function () {
        var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["Headers"]();
        this.loadToken();
        headers.append('Authorization', this.authToken);
        headers.append('Content-Type', 'application/json');
        return this.http.get('http://localhost:3000/users/profile', { headers: headers })
            .map(function (res) { return res.json(); });
    };
    AuthService.prototype.getNotEnabledusers = function () {
        var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["Headers"]();
        this.loadToken();
        headers.append('Authorization', this.authToken);
        headers.append('Content-Type', 'application/json');
        return this.http.get('http://localhost:3000/users/getNotEnabledUsers', { headers: headers })
            .map(function (res) { return res.json(); });
    };
    AuthService.prototype.deleteUser = function (userId) {
        var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["Headers"]();
        headers.append('Authorization', this.authToken);
        headers.append('Content-Type', 'application/json');
        var body = { userId: userId };
        return this.http.post('http://localhost:3000/users/deleteUser', body, { headers: headers })
            .map(function (res) { return res.json(); });
    };
    AuthService.prototype.enableUser = function (userId) {
        var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["Headers"]();
        headers.append('Authorization', this.authToken);
        headers.append('Content-Type', 'application/json');
        var body = { userId: userId };
        return this.http.post('http://localhost:3000/users/enableUser', body, { headers: headers })
            .map(function (res) { return res.json(); });
    };
    AuthService.prototype.storeUserData = function (token, user) {
        localStorage.setItem('id_token', token);
        localStorage.setItem('user', JSON.stringify(user));
        this.authToken = token;
        this.user = user;
    };
    AuthService.prototype.loadToken = function () {
        var token = localStorage.getItem('id_token');
        this.authToken = token;
    };
    AuthService.prototype.loggedIn = function () {
        return Object(__WEBPACK_IMPORTED_MODULE_3_angular2_jwt__["tokenNotExpired"])('id_token');
    };
    AuthService.prototype.logout = function () {
        this.authToken = null;
        this.user = null;
        localStorage.clear();
    };
    AuthService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_http__["Http"]])
    ], AuthService);
    return AuthService;
}());



/***/ }),

/***/ "../../../../../src/app/services/data.service.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DataService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_http__ = __webpack_require__("../../../http/esm5/http.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map__ = __webpack_require__("../../../../rxjs/_esm5/add/operator/map.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__auth_service__ = __webpack_require__("../../../../../src/app/services/auth.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var DataService = (function () {
    function DataService(http, authService) {
        this.http = http;
        this.authService = authService;
        this.initService();
    }
    DataService.prototype.initService = function () {
        var _this = this;
        this.getAllCategories().subscribe(function (cats) {
            _this.currentCategory = cats.cats[0];
            console.log("current category initially set to: " + _this.currentCategory.name);
        });
        this.authService.getProfile().subscribe(function (profile) {
            _this.user = profile.user;
        }, function (err) {
            console.log(err);
            return false;
        });
    };
    DataService.prototype.getAllEntries = function () {
        var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["Headers"]();
        this.authService.loadToken();
        headers.append('Authorization', this.authService.authToken);
        headers.append('Content-Type', 'application/json');
        return this.http.get('http://localhost:3000/rss/getAllEntries', { headers: headers })
            .map(function (res) { return res.json(); });
    };
    DataService.prototype.getEntries = function (dismissed, relevant, collected, category) {
        var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["Headers"]();
        this.authService.loadToken();
        headers.append('Authorization', this.authService.authToken);
        headers.append('Content-Type', 'application/json');
        var body = { dismissed: dismissed, relevant: relevant, collected: collected, category: category };
        return this.http.post('http://localhost:3000/rss/getEntries', body, { headers: headers })
            .map(function (res) { return res.json(); });
    };
    DataService.prototype.markAsDismissed = function (entry_id) {
        var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["Headers"]();
        this.authService.loadToken();
        headers.append('Authorization', this.authService.authToken);
        headers.append('Content-Type', 'application/json');
        var body = { entry_id: entry_id };
        return this.http.post('http://localhost:3000/rss/markAsDismissed', body, { headers: headers })
            .map(function (res) { return res.json(); });
    };
    DataService.prototype.markAsRelevant = function (entry_id) {
        var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["Headers"]();
        this.authService.loadToken();
        headers.append('Authorization', this.authService.authToken);
        headers.append('Content-Type', 'application/json');
        var body = { entry_id: entry_id };
        return this.http.post('http://localhost:3000/rss/markAsRelevant', body, { headers: headers })
            .map(function (res) { return res.json(); });
    };
    DataService.prototype.addEntryToCollected = function (entry_id, collected_data) {
        var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["Headers"]();
        this.authService.loadToken();
        headers.append('Authorization', this.authService.authToken);
        headers.append('Content-Type', 'application/json');
        var body = { data: collected_data, entry_id: entry_id, user: this.user };
        return this.http.post('http://localhost:3000/collected/addEntry', body, { headers: headers })
            .map(function (res) { return res.json(); });
    };
    DataService.prototype.getCollectedEntries = function (exported) {
        var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["Headers"]();
        this.authService.loadToken();
        headers.append('Authorization', this.authService.authToken);
        headers.append('Content-Type', 'application/json');
        var body = { exported: exported };
        return this.http.post('http://localhost:3000/collected/getEntries', body, { headers: headers })
            .map(function (res) { return res.json(); });
    };
    DataService.prototype.getAllCategories = function () {
        var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["Headers"]();
        this.authService.loadToken();
        headers.append('Authorization', this.authService.authToken);
        headers.append('Content-Type', 'application/json');
        return this.http.get('http://localhost:3000/category/getAllEntries', { headers: headers })
            .map(function (res) { return res.json(); });
    };
    DataService.prototype.addNewCategory = function (category_title) {
        var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["Headers"]();
        this.authService.loadToken();
        headers.append('Authorization', this.authService.authToken);
        headers.append('Content-Type', 'application/json');
        var body = { category_title: category_title, creator: "tester" };
        return this.http.post('http://localhost:3000/category/addEntry', body, { headers: headers })
            .map(function (res) { return res.json(); });
    };
    DataService.prototype.getAllSourcesOfCategory = function (category_id) {
        var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["Headers"]();
        this.authService.loadToken();
        headers.append('Authorization', this.authService.authToken);
        headers.append('Content-Type', 'application/json');
        var body = { category_id: category_id };
        return this.http.post('http://localhost:3000/source/getAllEntriesOfCategory', body, { headers: headers })
            .map(function (res) { return res.json(); });
    };
    DataService.prototype.addNewSourcetoCategory = function (source_url, category_id) {
        var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["Headers"]();
        this.authService.loadToken();
        headers.append('Authorization', this.authService.authToken);
        headers.append('Content-Type', 'application/json');
        var body = { url: source_url, category: category_id, added_by_user: "tester" };
        return this.http.post('http://localhost:3000/source/addEntry', body, { headers: headers })
            .map(function (res) { return res.json(); });
    };
    DataService.prototype.setCurrentCategory = function (new_current_category) {
        //storing the entire object would result in getting just a string back!
        /*
        localStorage.setItem('currentCategoryID', new_current_category._id);
        localStorage.setItem('currentCategoryName', new_current_category.name);
        localStorage.setItem('currentCategoryCreator', new_current_category.creator);
        localStorage.setItem('currentCategoryDate', new_current_category.created_date);
        */
        this.currentCategory = new_current_category;
    };
    DataService.prototype.getCurrentCategory = function () {
        /*
        let currentCatID =  localStorage.getItem('currentCategory');
        let currentCatName =  localStorage.getItem('currentCategoryName');
        let currentCatCreator =  localStorage.getItem('currentCategoryCreator');
        let currentCatDate =  localStorage.getItem('currentCategoryDate');
    
        if(!currentCatID || ! !currentCatName || !currentCatCreator || !currentCatDate) {
          this.getAllCategories().subscribe(
            cats => {
              let currentCat = cats.cats[0];
              this.setCurrentCategory(currentCat);
              console.log("getCurrentCategory will return:");
              console.log(currentCat);
              return currentCat;
            }
          )
        } else {
          let currentCat:currentCategory;
          currentCat._id = currentCatID;
          currentCat.name = currentCatName;
          currentCat.creator = currentCatCreator;
          currentCat.created_date = currentCatDate;
    
          console.log("getCurrentCategory will return:");
          console.log(currentCat);
    
          return currentCat;
        }
        */
        return this.currentCategory;
    };
    DataService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_http__["Http"],
            __WEBPACK_IMPORTED_MODULE_3__auth_service__["a" /* AuthService */]])
    ], DataService);
    return DataService;
}());



/***/ }),

/***/ "../../../../../src/app/services/validate.service.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ValidateService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var ValidateService = (function () {
    function ValidateService() {
    }
    ValidateService.prototype.validateRegister = function (user) {
        if (user.name == undefined || user.email == undefined || user.username == undefined || user.password == undefined) {
            return false;
        }
        else {
            return true;
        }
    };
    ValidateService.prototype.validateEmail = function (email) {
        //from: https://stackoverflow.com/questions/46155/how-to-validate-email-address-in-javascript but with const instead of var
        var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(email);
    };
    ValidateService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [])
    ], ValidateService);
    return ValidateService;
}());



/***/ }),

/***/ "../../../../../src/environments/environment.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return environment; });
// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.
var environment = {
    production: false
};


/***/ }),

/***/ "../../../../../src/main.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_platform_browser_dynamic__ = __webpack_require__("../../../platform-browser-dynamic/esm5/platform-browser-dynamic.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__app_app_module__ = __webpack_require__("../../../../../src/app/app.module.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__environments_environment__ = __webpack_require__("../../../../../src/environments/environment.ts");




if (__WEBPACK_IMPORTED_MODULE_3__environments_environment__["a" /* environment */].production) {
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["enableProdMode"])();
}
Object(__WEBPACK_IMPORTED_MODULE_1__angular_platform_browser_dynamic__["a" /* platformBrowserDynamic */])().bootstrapModule(__WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */])
    .catch(function (err) { return console.log(err); });


/***/ }),

/***/ 0:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__("../../../../../src/main.ts");


/***/ })

},[0]);
//# sourceMappingURL=main.bundle.js.map