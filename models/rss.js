const mongoose = require('mongoose');
const config = require('../config/database');

// RSS schema
const RSSSchema = mongoose.Schema({
  title: {
    type: String,
    required: true
  },
  link: {
    type: String,
    required: true
  },
  description: {
    type: String,
    required: true
  },
  pubDate: {
    type: String
  },
  author: {
    type: String
  },
  contributors: {
    type: String
  },
  publisher: {
    type: String
  },
  created: {
    type: Date
  },
  published: {
    type: Date
  },
  rss_id: {
    type: String
  },
  source: {
    type: String
  },
  category: {
    type: String
  },
  datetime_crawled_at: {
    type: String
  },
  tags: {
    type: String
  },
  dismissed: {
    type: Boolean,
    default: false
  },
  dismissedDate: {
    type: Date
  },
  relevant: {
    type: Boolean,
    default: false
  },
  relevantDate: {
    type: Date
  },
  collected: {
    type: Boolean,
    default: false
  }
});

const RSS = module.exports = mongoose.model('RSS', RSSSchema);

module.exports.getEntryById = function(id, callback) {
  RSS.findById(id, callback);
}

module.exports.getAllEntries = function (callback) {
  RSS.find({}, callback);
}

module.exports.getEntries = function(dismissed, relevant, collected, category, callback) {
  RSS.find({'dismissed': dismissed, 'relevant': relevant, 'collected': collected, 'category': category}, callback);
}

module.exports.addEntry = function(newEntry, callback) {
  newEntry.save(callback);
}

module.exports.markAsDismissed = function(entry_id, callback) {
  RSS.update({_id: entry_id}, { dismissed: true, relevant: false }, callback);
}

module.exports.markAsRelevant = function(entry_id, callback) {
  RSS.update({_id: entry_id}, { relevant: true, dismissed: false }, callback);
}

module.exports.markAsCollected = function(entry_id, callback) {
  RSS.update({_id: entry_id}, { collected: true }, callback);
}
