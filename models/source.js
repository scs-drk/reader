const mongoose = require('mongoose');
const config = require('../config/database');

// Source schema
const SourceSchema = mongoose.Schema({
  url: {
    type: String,
    required: true
  },
  category: {
    type: String,
    required: true
  },
  added_date: {
    type: Date,
    default: Date.now
  },
  added_by_user: {
    type: String,
    required: true
  }
});

const Source = module.exports = mongoose.model('Source', SourceSchema);

module.exports.getEntryById = function(id, callback) {
  Source.findById(id, callback);
}

module.exports.getAllEntries = function (callback) {
  Source.find({}, callback);
}

module.exports.getAllEntriesOfCategory = function (category_id, callback) {
  Source.find({category: category_id}, callback);
}

module.exports.addEntry = function(newEntry, callback) {
  newEntry.save(callback);
}
