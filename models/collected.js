const mongoose = require('mongoose');
const config = require('../config/database');

// Collected schema
const CollectedSchema = mongoose.Schema({
  news_type: {
    type: String
  },
  title: {
    type: String,
    required: true
  },
  date: {
    type: Date,
    required: true
  },
  author: {
    type: String
  },
  publisher: {
    type: String
  },
  abstract: {
    type: String
  },
  content: {
    type: String,
  },
  url: {
    type: String,
    required: true
  },
  application_field: {
    type: String
  },
  application: {
    type: String
  },
  user: {
    type: String
  },
  industry_user: {
    type: String
  },
  geography_user: {
    type: String
  },
  function: {
    type: String
  },
  application: {
    type: String
  },
  technology: {
    type: String
  },
  maturity_level: {
    type: String
  },
  technology_provider: {
    type: String
  },
  industry_provider: {
    type: String
  },
  geography_provider: {
    type: String
  },
  ecosystem: {
    type: String
  },
  application_trend: {
    type: String
  },
  technology_trend: {
    type: String
  },
  ecosystem_trend: {
    type: String
  },
  exported: {
    type: Boolean
  },
  picked_by: {
    type: String
  }
});

const Collected = module.exports = mongoose.model('Collected', CollectedSchema);

module.exports.addEntry = function(newEntry, callback) {
  newEntry.save(callback);
}

module.exports.getEntries = function(exported, callback) {
  Collected.find({'exported': exported}, callback);
}

module.exports.markAsExported = function(entry_id, callback) {
  Collected.update({_id: entry_id}, { exported: true }, callback);
}

module.exports.markMultipleAsExported = function(ids, callback) {
  Collected.update(
    { _id: { $in: ids }},
    { $set: { exported: true }},
    { multi: true },
    callback
  );
}
