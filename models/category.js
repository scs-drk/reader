const mongoose = require('mongoose');
const config = require('../config/database');

// Category schema
const CategorySchema = mongoose.Schema({
  name: {
    type: String,
    required: true
  },
  created_date: {
    type: Date,
    default: Date.now
  },
  creator: {
    type: String,
    required: true
  }
});

const Category = module.exports = mongoose.model('Category', CategorySchema);

module.exports.getEntryById = function(id, callback) {
  Category.findById(id, callback);
}

module.exports.getAllEntries = function (callback) {
  Category.find({}, callback);
}

module.exports.addEntry = function(newEntry, callback) {
  newEntry.save(callback);
}
