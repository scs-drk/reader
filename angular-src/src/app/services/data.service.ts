import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import 'rxjs/add/operator/map';
import { tokenNotExpired } from 'angular2-jwt';
import { AuthService } from './auth.service';


interface currentCategory {
  _id: String;
  name: String;
  created_date: String;
  creator: String;
}

@Injectable()
export class DataService {

  user: Object;
  currentCategory;

  constructor(
    private http: Http,
    private authService: AuthService
  ) {
    this.initService();
  }

  initService() {
    this.getAllCategories().subscribe(
      cats => {
        this.currentCategory = cats.cats[0];
        console.log("current category initially set to: " + this.currentCategory.name);
      }
    )

    this.authService.getProfile().subscribe(profile => {
      this.user = profile.user;
    },
    err => {
      console.log(err);
      return false;
    })
  }

  getAllEntries() {
    let headers = new Headers();
    this.authService.loadToken();
    headers.append('Authorization', this.authService.authToken);
    headers.append('Content-Type', 'application/json');
    return this.http.get('http://localhost:3000/rss/getAllEntries', {headers: headers})
      .map(res => res.json());
  }

  getEntries(dismissed, relevant, collected, category) {
    let headers = new Headers();
    this.authService.loadToken();
    headers.append('Authorization', this.authService.authToken);
    headers.append('Content-Type', 'application/json');
    const body = {dismissed: dismissed, relevant: relevant, collected: collected, category: category};
    return this.http.post('http://localhost:3000/rss/getEntries', body, {headers: headers})
      .map(res => res.json());
  }

  markAsDismissed(entry_id) {
    let headers = new Headers();
    this.authService.loadToken();
    headers.append('Authorization', this.authService.authToken);
    headers.append('Content-Type', 'application/json');
    const body = {entry_id: entry_id};
    return this.http.post('http://localhost:3000/rss/markAsDismissed', body, {headers: headers})
      .map(res => res.json());
  }

  markAsRelevant(entry_id) {
    let headers = new Headers();
    this.authService.loadToken();
    headers.append('Authorization', this.authService.authToken);
    headers.append('Content-Type', 'application/json');
    const body = {entry_id: entry_id};
    return this.http.post('http://localhost:3000/rss/markAsRelevant', body, {headers: headers})
      .map(res => res.json());
  }

  addEntryToCollected(entry_id, collected_data) {
    let headers = new Headers();
    this.authService.loadToken();
    headers.append('Authorization', this.authService.authToken);
    headers.append('Content-Type', 'application/json');
    const body = {data: collected_data, entry_id: entry_id, user: this.user};
    return this.http.post('http://localhost:3000/collected/addEntry', body, {headers:headers})
      .map(res => res.json());
  }

  getCollectedEntries(exported) {
    let headers = new Headers();
    this.authService.loadToken();
    headers.append('Authorization', this.authService.authToken);
    headers.append('Content-Type', 'application/json');
    const body = {exported: exported};
    return this.http.post('http://localhost:3000/collected/getEntries', body, {headers:headers})
      .map(res => res.json());
  }

  markCollectedEntriesExported(ids) {
    let headers = new Headers();
    this.authService.loadToken();
    headers.append('Authorization', this.authService.authToken);
    headers.append('Content-Type', 'application/json');
    const body = {ids: ids};
    return this.http.post('http://localhost:3000/collected/markEntriesAsExported', body, {headers:headers})
      .map(res => res.json());
  }

  getAllCategories() {
    let headers = new Headers();
    this.authService.loadToken();
    headers.append('Authorization', this.authService.authToken);
    headers.append('Content-Type', 'application/json');
    return this.http.get('http://localhost:3000/category/getAllEntries', {headers:headers})
      .map(res => res.json());
  }

  addNewCategory(category_title) {
    let headers = new Headers();
    this.authService.loadToken();
    headers.append('Authorization', this.authService.authToken);
    headers.append('Content-Type', 'application/json');
    const body = {category_title: category_title, creator: "tester"};
    return this.http.post('http://localhost:3000/category/addEntry', body, {headers: headers})
      .map(res => res.json());
  }

  getAllSourcesOfCategory(category_id) {
    let headers = new Headers();
    this.authService.loadToken();
    headers.append('Authorization', this.authService.authToken);
    headers.append('Content-Type', 'application/json');
    const body = {category_id: category_id};
    return this.http.post('http://localhost:3000/source/getAllEntriesOfCategory', body, {headers: headers})
      .map(res => res.json());
  }

  addNewSourcetoCategory(source_url, category_id) {
    let headers = new Headers();
    this.authService.loadToken();
    headers.append('Authorization', this.authService.authToken);
    headers.append('Content-Type', 'application/json');
    const body = {url: source_url, category: category_id, added_by_user: "tester"};
    return this.http.post('http://localhost:3000/source/addEntry', body, {headers: headers})
      .map(res => res.json());
  }

  setCurrentCategory(new_current_category) {
    //storing the entire object would result in getting just a string back!
    /*
    localStorage.setItem('currentCategoryID', new_current_category._id);
    localStorage.setItem('currentCategoryName', new_current_category.name);
    localStorage.setItem('currentCategoryCreator', new_current_category.creator);
    localStorage.setItem('currentCategoryDate', new_current_category.created_date);
    */
    this.currentCategory = new_current_category;
  }

  getCurrentCategory() {
    /*
    let currentCatID =  localStorage.getItem('currentCategory');
    let currentCatName =  localStorage.getItem('currentCategoryName');
    let currentCatCreator =  localStorage.getItem('currentCategoryCreator');
    let currentCatDate =  localStorage.getItem('currentCategoryDate');

    if(!currentCatID || ! !currentCatName || !currentCatCreator || !currentCatDate) {
      this.getAllCategories().subscribe(
        cats => {
          let currentCat = cats.cats[0];
          this.setCurrentCategory(currentCat);
          console.log("getCurrentCategory will return:");
          console.log(currentCat);
          return currentCat;
        }
      )
    } else {
      let currentCat:currentCategory;
      currentCat._id = currentCatID;
      currentCat.name = currentCatName;
      currentCat.creator = currentCatCreator;
      currentCat.created_date = currentCatDate;

      console.log("getCurrentCategory will return:");
      console.log(currentCat);

      return currentCat;
    }
    */
    return this.currentCategory;
  }

}
