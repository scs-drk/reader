import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { RouterModule, Routes } from '@angular/router';
import { HttpModule } from '@angular/http';


import { AppComponent } from './app.component';
import { NavbarComponent } from './components/navbar/navbar.component';
import { LoginComponent } from './components/login/login.component';
import { RegisterComponent } from './components/register/register.component';
import { HomeComponent } from './components/home/home.component';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { ProfileComponent } from './components/profile/profile.component';
import { PickerComponent } from './components/picker/picker.component';
import { FeedComponent } from './components/feed/feed.component';
import { CategoriesComponent } from './components/categories/categories.component';
import { SourcesComponent } from './components/sources/sources.component';

import { ValidateService } from './services/validate.service';
import { AuthService } from './services/auth.service';
import { DataService } from './services/data.service';
import { FlashMessagesModule } from 'angular2-flash-messages';
import { AuthGuard } from './guards/auth.guard';



const appRoutes: Routes = [
  {path:'', component: HomeComponent, pathMatch: 'full'},
  {path:'register', component: RegisterComponent, pathMatch: 'full'},
  {path:'login', component: LoginComponent, pathMatch: 'full'},
  {path:'feed', component: FeedComponent, pathMatch: 'full', canActivate:[AuthGuard]},
  {path:'dashboard', component: DashboardComponent, pathMatch: 'full', canActivate:[AuthGuard]},
  {path:'profile', component: ProfileComponent, pathMatch: 'full', canActivate:[AuthGuard]},
  {path:'picker', component: PickerComponent, pathMatch: 'full'},
  {path:'categories', component: CategoriesComponent, pathMatch: 'full', canActivate:[AuthGuard]},
  {path:'sources', component: SourcesComponent, pathMatch: 'full', canActivate:[AuthGuard]}
]

@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    LoginComponent,
    RegisterComponent,
    HomeComponent,
    DashboardComponent,
    ProfileComponent,
    PickerComponent,
    FeedComponent,
    CategoriesComponent,
    SourcesComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    RouterModule.forRoot(appRoutes),
    FlashMessagesModule.forRoot(),
    HttpModule
  ],
  providers: [ValidateService, AuthService, AuthGuard, DataService],
  bootstrap: [AppComponent]
})
export class AppModule { }
