import { Component, OnInit } from '@angular/core';
import { DataService } from '../../services/data.service';
import { FlashMessagesService } from 'angular2-flash-messages';


@Component({
  selector: 'app-picker',
  templateUrl: './picker.component.html',
  styleUrls: ['./picker.component.css']
})

export class PickerComponent implements OnInit {

  rss;
  currentEntry;
  data;
  compactModeActivated = false;

  news_type: String;
  title: String;
  date: Date;
  author: String;
  publisher: String;
  abstract: String;
  content: String;
  url: URL;
  application_field: String;
  application: String;
  user: String;
  industry_user: String;
  geography_user: String;
  function: String;
  form_application: String;
  technology: String;
  maturity_level: String;
  technology_provider: String;
  industry_provider: String;
  geography_provider: String;
  ecosystem: String;
  application_trend: String;
  technology_trend: String;
  ecosystem_trend: String;

  constructor(
    private dataService: DataService,
    private _flashMessagesService: FlashMessagesService
  ) { }

  ngOnInit() {
    this.setUpPicker();
  }

  setUpPicker() {
    this.dataService.getEntries(false, true, false, this.dataService.getCurrentCategory()._id).subscribe(
      rss => {
        this.rss = rss.rss; //take only array not entire object
        this.setCurrentEntry(this.rss[0]);
        console.log(rss);
        console.log(this.currentEntry);
      }
    );
  }

  setCurrentEntry(entry) {
    localStorage.setItem("current_entry", entry);
    this.currentEntry = entry;
    document.getElementById("myiframe").setAttribute("src", entry.link);
    document.getElementById("mybutton").setAttribute("href", entry.link);
    this.title = entry.title;
    this.url = entry.link;
    this.abstract = entry.summary;
    this.content = entry.summary;
  }

  submitCollectedEntry() {
    const collected_data = {
      news_type: this.news_type,
      title: this.title,
      date: this.date,
      author: this.author,
      publisher: this.publisher,
      abstact: this.abstract,
      content: this.content,
      url: this.url,
      application_field: this.application_field,
      application: this.application,
      user: this.user,
      industry_user: this.industry_user,
      geography_user: this.geography_user,
      function: this.function,
      form_application: this.application,
      technology: this.technology,
      maturity_level: this.maturity_level,
      technology_provider: this.technology_provider,
      industry_provider: this.industry_provider,
      geography_provider: this.geography_provider,
      ecosystem: this.ecosystem,
      application_trend: this.application_trend,
      technology_trend: this.technology_trend,
      ecosystem_trend: this.ecosystem_trend
    }

    console.log("this entry should be submitted: " + this.currentEntry._id);
    this.dataService.addEntryToCollected(this.currentEntry._id, collected_data).subscribe(
      data => {
        if(data.success) {
          this._flashMessagesService.show('Data saved successfully', {cssClass: 'alert-success', timeout: 8000});
          this.setUpPicker();
        } else {
          console.log("entry could not be added");
          this._flashMessagesService.show('Something went wrong', {cssClass: 'alert-danger', timeout: 8000});
        }
      }
    )
  }

  //same as in feed.component.ts
  markAsDismissed(entry_id) {
    this.dataService.markAsDismissed(entry_id).subscribe(
      res => {
        if(res.success) {
          this.removeEntryCard(entry_id);
        } else {
          console.log("Entry could not be dismissed");
        }
      }
    );
  }

  removeEntryCard(entry_id) {
    document.getElementById(entry_id).remove();
    this.setUpPicker();
  };



}
