import { Component, OnInit } from '@angular/core';
import { DataService } from '../../services/data.service';


@Component({
  selector: 'app-categories',
  templateUrl: './categories.component.html',
  styleUrls: ['./categories.component.css']
})
export class CategoriesComponent implements OnInit {

  categories;

  newCategoryTitle: String;

  constructor(
    private dataService: DataService,
  ) { }

  ngOnInit() {
    this.listAllCategories();
  }

  listAllCategories() {
    this.dataService.getAllCategories().subscribe(
      cats => {
        this.categories = cats.cats;
        console.log(cats.cats);
      }
    );
  }

  addNewCategory() {
    console.log(this.newCategoryTitle);
    this.dataService.addNewCategory(this.newCategoryTitle).subscribe(
      res => {
        if(res.success) {
          this.newCategoryTitle = "";
          this.listAllCategories();
        } else {
          console.log("Entry could not be added");
        }
      }
    )
  }

  setAsCurrentCategory(category) {
    this.dataService.setCurrentCategory(category);
  }


}
