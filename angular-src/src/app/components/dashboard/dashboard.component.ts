import { Component, OnInit } from '@angular/core';
import { DataService } from '../../services/data.service';
import { Router } from '@angular/router';
import { FlashMessagesService } from 'angular2-flash-messages';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

  collected;
  exportableData;

  constructor(
    private dataService: DataService,
    private router: Router,
    private _flashMessagesService: FlashMessagesService
  ) { }

  ngOnInit() {
  }

  getExportedEntries() {
    this.getCollectedEntries(true);
  }

  getNotExportedEntries() {
    this.getCollectedEntries(false);
  }

  getCollectedEntries(exported) {
    this.collected = "";
    this.dataService.getCollectedEntries(exported).subscribe(
      collected => {
        this.collected = collected.collected;
        console.log(collected);
      }
    )
  }

  exportDisplayedData() {
    this.markEntriesExported();
    let exportable = this.enrichDataObjectWithEmptyFields();
    this.exportToCSV(exportable);
  }

  markEntriesExported() {
    let ids = [];
    for (let i = 0; i < this.collected.length; i++) {
      let id = this.collected[i]._id;
      ids.push(id);
    }

    this.dataService.markCollectedEntriesExported(ids).subscribe(
      res => {
        console.log(res.msg);
        if(res.success) {
          window.location.reload();
        } else {
          this._flashMessagesService.show('Exported entries could not be marked exported in database', {cssClass: 'alert-danger', timeout: 5000});
        }
      }
    )
  }

  enrichDataObjectWithEmptyFields() {
    let sourceData = this.collected;
    let JSONobject = [];
    let numberOfEntries = this.collected.length;
    for (let i = 0; i < numberOfEntries; i++) {
      let entryObject = this.dbObjectToCompleteObject(sourceData[i]);
      JSONobject.push(entryObject)
    }
    return JSONobject;
  }

  dbObjectToCompleteObject(dataObject) {
    var newObject = <any>{};
    newObject.type = dataObject.news_type || "";
    newObject.title = dataObject.title || "";
    newObject.date = dataObject.date || "";
    newObject.author = dataObject.author || "";
    newObject.publisher = dataObject.publisher || "";
    newObject.abstract = dataObject.abstract || "";
    newObject.content = dataObject.content || "";
    newObject.url = dataObject.url || "";
    newObject.attachmentUrl = dataObject.attachmentUrl || "";   //??
    newObject.applicationField = dataObject.application_field || "";
    newObject.application = dataObject.application || "";
    newObject.user = dataObject.user || "";
    newObject.userIndustry = dataObject.industry_user || "";
    newObject.userGeography = dataObject.geography_user || "";
    newObject.function = dataObject.function || "";
    newObject.application = dataObject.application || "";
    newObject.technology = dataObject.technology || "";
    newObject.maturity_level = dataObject.maturity_level || "";
    newObject.technology_provider = dataObject.technology_provider || "";
    newObject.providerGeography = dataObject.geography_provider || "";
    newObject.ecosystem = dataObject.ecosystem || "";
    newObject.application_trend = dataObject.application_trend || "";
    newObject.technology_trend = dataObject.technology_trend || "";
    newObject.ecosystem_trend = dataObject.ecosystem_trend || "";
    return newObject;
  }

  // https://stackoverflow.com/questions/18848860/javascript-array-to-csv/18849208#18849208
  objectToCSVRow(dataObject) {
    var dataArray = new Array;
    for (var o in dataObject) {
        var innerValue = dataObject[o]===null?'':dataObject[o].toString();
        var result = innerValue.replace(/"/g, '""');
        result = '"' + result + '"';
        dataArray.push(result);
    }
    return dataArray.join(' ') + '\r\n';
  }

  // https://stackoverflow.com/questions/18848860/javascript-array-to-csv/18849208#18849208
  exportToCSV(arrayOfObjects) {

      if (!arrayOfObjects.length) {
          return;
      }

      var csvContent = "data:text/csv;charset=utf-8,";

      // headers
      csvContent += this.objectToCSVRow(Object.keys(arrayOfObjects[0]));

      for (let item of arrayOfObjects) {
        csvContent += this.objectToCSVRow(item);
      }

      let fileName = "deepReadRSSexport_" + this.getDate() + ".csv";

      var encodedUri = encodeURI(csvContent);
      var link = document.createElement("a");
      link.setAttribute("href", encodedUri);
      link.setAttribute("download", fileName);
      document.body.appendChild(link); // Required for FF
      link.click();
      document.body.removeChild(link);
  }

  getDate() {
    var today = new Date();
    var d = today.getDate();
    var m = today.getMonth()+1;
    var yyyy = today.getFullYear();
    var dd;
    var mm;
    if(d < 10) {
      dd = '0'+d
    } else {
      dd = d;
    }
    if(m < 10) {
      mm = '0'+m
    } else {
      mm = m;
    }

    return yyyy + mm + dd;
  }
}
