import { Component, OnInit } from '@angular/core';
import { DataService } from '../../services/data.service';
import { Router } from '@angular/router';


@Component({
  selector: 'app-feed',
  templateUrl: './feed.component.html',
  styleUrls: ['./feed.component.css']
})
export class FeedComponent implements OnInit {

  rss;
  entry_adjective = "";
  showsNewEnries = true;

  constructor(
    private dataService: DataService,
    private router: Router
  ) { }

  ngOnInit() {
    this.fillFeedForCurrentCategory();
  }

  fillFeedForCurrentCategory() {
    this.showsNewEnries = true;
    this.entry_adjective = "new";
    this.rss = "";
    let cat = this.dataService.getCurrentCategory();
    this.dataService.getEntries(false, false, false, cat._id).subscribe(
      rss => {
        this.rss = rss.rss; //take only array not entire object
        console.log(rss);
      }
    );
  }

  fillFeedForCurrentCategoryWithDismissedEntries() {
    this.showsNewEnries = false;
    this.entry_adjective = "dismissed";
    this.rss = "";
    let cat = this.dataService.getCurrentCategory();
    this.dataService.getEntries(true, false, false, cat._id).subscribe(
      rss => {
        this.rss = rss.rss; //take only array not entire object
        console.log(rss);
      }
    );
  }

  markAsDismissed(entry_id) {
    this.dataService.markAsDismissed(entry_id).subscribe(
      res => {
        if(res.success) {
          this.removeEntryCard(entry_id);
        } else {
          console.log("Entry could not be dismissed");
        }
      }
    );
  }

  markAsRelevant(entry_id) {
    this.dataService.markAsRelevant(entry_id).subscribe(
      res => {
        if(res.success) {
          this.removeEntryCard(entry_id);
        } else {
          console.log("Entry could not be marked as relevant");
        }
      }
    );
  }

  removeEntryCard(entry_id) {
    document.getElementById(entry_id).remove();
  }

}
