import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../services/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {
  user: Object;
  notEnabledUsers: Object;
  usersToEnable: Boolean;

  constructor(
    private authService: AuthService,
    private router: Router
  ) { }

  ngOnInit() {
    this.usersToEnable = false;
    this.authService.getProfile().subscribe(profile => {
      this.user = profile.user;
    },
    err => {
      console.log(err);
      return false;
    })

    this.authService.getNotEnabledusers().subscribe(users => {
      if(users.success) {
        this.notEnabledUsers = users.users;
        if(users.users.length > 0) {
          this.usersToEnable = true;
        }
      }
    })
  }

  enableUser(user_id) {
    console.log("try to enable user with id ", user_id)
    this.authService.enableUser(user_id).subscribe(res => {
      if(res.success) {
        console.log("user enabled");
        this.ngOnInit();
      } else {
        console.log("user could not be enabled");
      }
    })
  }

  deleteUser(user_id) {
    console.log("try to delete user with id ", user_id)
    this.authService.deleteUser(user_id).subscribe(res => {
      if(res.success) {
        console.log("user deleted");
        this.ngOnInit();
      } else {
        console.log("user could not be deleted");
      }
    })
  }

}
