import { Component, OnInit } from '@angular/core';
import { DataService } from '../../services/data.service';


@Component({
  selector: 'app-sources',
  templateUrl: './sources.component.html',
  styleUrls: ['./sources.component.css']
})
export class SourcesComponent implements OnInit {

  sources;

  newSourceUrl: String;

  constructor(
    private dataService: DataService,
  ) { }

  ngOnInit() {
    this.listAllSources();
  }

  //lists all sources of dataService.currentCategory
  listAllSources() {
    this.dataService.getAllSourcesOfCategory(this.dataService.currentCategory._id).subscribe(
      sources => {
        this.sources = sources.sources;
      }
    )
  }

  addNewSource() {
    console.log("Add new source in source component called with arguments: " + this.newSourceUrl + " and " + this.dataService.currentCategory._id);
    this.dataService.addNewSourcetoCategory(this.newSourceUrl, this.dataService.currentCategory._id).subscribe(
      res => {
        if(res.success) {
          this.newSourceUrl = "";
          this.listAllSources();
        } else {
          console.log("Entry could not be added");
        }
      }
    )
  }

}
