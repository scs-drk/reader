const express = require('express');
const router = express.Router();
const passport = require('passport');
const config = require('../config/database');

const category = require('../models/category');


// getAllEntries
router.get('/getAllEntries', passport.authenticate('jwt', {session:false}), function(req, res, next) {
  category.getAllEntries(function(err, cats) {
    if(err) {
      res.json({success: false, msg:'Could not get entries'});
    } else {
      res.json({cats:cats});
    }
  });
});


// addEntry
router.post('/addEntry', function(req, res, next) {

  let newCategory = new category({
    name: req.body.category_title,
    creator: req.body.creator
  });
  console.log("add Entry in category called");

  category.addEntry(newCategory, function(err, rss) {
    if(err) {
      res.json({success: false, msg:'Failed to add entry'});
    } else {
      res.json({success: true, msg:'Entry added successfully'});
    }
  });

});

module.exports = router;
