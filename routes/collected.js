const express = require('express');
const router = express.Router();
const passport = require('passport');
const config = require('../config/database');

const collected = require('../models/collected');
const RSS = require('../models/rss');


router.post('/addEntry', function(req, res, next) {
  let newCollected = new collected({
    news_type: req.body.data.news_type,
    title: req.body.data.title,
    date: req.body.data.date,
    author: req.body.data.author,
    publisher: req.body.data.publisher,
    abstract: req.body.data.abstract,
    content: req.body.data.content,
    url: req.body.data.url,
    application_field: req.body.data.application_field,
    application: req.body.data.application,
    user: req.body.data.user,
    industry_user: req.body.data.industry_user,
    geography_user: req.body.data.geography_user,
    function: req.body.data.function,
    application: req.body.data.application,
    technology: req.body.data.technology,
    maturity_level: req.body.data.maturity_level,
    technology_provider: req.body.data.technology_provider,
    industry_provider: req.body.data.industry_provider,
    geography_provider: req.body.data.geography_provider,
    ecosystem: req.body.data.ecosystem,
    application_trend: req.body.data.application_trend,
    technology_trend: req.body.data.technology_trend,
    ecosystem_trend: req.body.data.ecosystem_trend,
    exported: false,
    picked_by: req.body.user.username
  });
  //The _id of the entry in the rss collection
  let entry_id = req.body.entry_id;

  collected.addEntry(newCollected, function(err, rss) {
    if(err) {
      res.json({success: false, msg:'Failed to add entry'});
    } else {
      RSS.markAsCollected(entry_id, function(err, rss) {
        if(err) {
          res.json({success: false, msg: 'Entry added successfully to collected, but was not marked as collected in rss'})
        } else {
          res.json({success: true, msg:'Entry added successfully'});
        }
      });
    }
  });

});

router.post('/getEntries', passport.authenticate('jwt', {session: false}), function(req, res, next) {
  let exported = req.body.exported;

  collected.getEntries(exported, function(err, collected) {
    if(err) {
      res.json({success:false, msg: 'could not get entries'});
    } else {
      res.json({collected: collected});
    }
  })

});

router.post('/markEntriesAsExported', passport.authenticate('jwt', {session: false}), function(req, res, next) {
  let ids = req.body.ids;

  collected.markMultipleAsExported(ids, function(err) {
    if(err) {
      res.json({success:false, msg: 'could not mark entries as exported'});
    } else {
      res.json({success:true, msg: 'entries marked as exported'});
    }
  })

});



module.exports = router;
