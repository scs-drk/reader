const express = require('express');
const router = express.Router();
const passport = require('passport');
const config = require('../config/database');

const source = require('../models/source');


// getAllEntries
router.post('/getAllEntriesOfCategory', passport.authenticate('jwt', {session:false}), function(req, res, next) {
  let category_id = req.body.category_id;

  source.getAllEntriesOfCategory(category_id, function(err, sources) {
    if(err) {
      res.json({success: false, msg:'Could not get entries'});
    } else {
      res.json({sources: sources});
    }
  });
});


// addEntry
router.post('/addEntry', function(req, res, next) {

  let newSource = new source({
    url: req.body.url,
    category: req.body.category,
    added_by_user: req.body.added_by_user
  });

  source.addEntry(newSource, function(err, rss) {
    if(err) {
      console.log("Failed to add newSource");
      console.log(err);
      res.json({success: false, msg:'Failed to add entry'});
    } else {
      res.json({success: true, msg:'Entry added successfully'});
    }
  });

});

module.exports = router;
