const express = require('express');
const router = express.Router();
const passport = require('passport');
const jwt = require('jsonwebtoken');
const config = require('../config/database');

const User = require('../models/user');

// Register
router.post('/register', function(req, res, next) {
  let newUser = new User({
    name: req.body.name,
    email: req.body.email,
    username: req.body.username,
    password: req.body.password,
    enabled: false
  });

  User.addUser(newUser, function(err, user) {
    if(err) {
      res.json({success: false, msg:'Failed to register user'});
    } else {
      res.json({success: true, msg:'User registered'});
    }
  });

});

// Authenticate
router.post('/authenticate', function(req, res, next) {
  const username = req.body.username;
  const password = req.body.password;

  User.getUserByUsername(username, function(err, user) {
    if(err) throw err;
    if(!user) {
      return res.json({success: false, msg: 'User not found'})
    }
    if(!user.enabled) {
      return res.json({success:false, msg: 'User not enabled. Contact you supervisor.'})
    }

    User.comparePassword(password, user.password, function(err, isMatch) {
      if(err) throw err;

      if(isMatch) {
        const token = jwt.sign({data: user}, config.secret, {
          expiresIn: 604800 // 1 week
        });

        res.json({
          success: true,
          token: 'JWT '+token,
          user: {
            id: user._id,
            name: user.name,
            username: user.username,
            email: user.email
          }
        });
      } else {
        return res.json({success: false, msg: 'Wrong password'});
      }
    })
  })
});

// Profile
router.get('/profile', passport.authenticate('jwt', {session:false}), function(req, res, next) {
  res.json({user:req.user});
});


router.get('/getNotEnabledUsers', passport.authenticate('jwt', {session:false}), function(req, res, next) {
  User.getNotEnabledUsers(function(err, users) {
    if(err) {
      res.json({success: false, msg:'Could not get users'});
    } else {
      res.json({success: true, users:users});
    }
  });
});

router.post('/deleteUser', passport.authenticate('jwt', {session:false}), function(req, res, next) {

  const userId = req.body.userId;

  User.deleteUser(userId, function(err, user) {
    if(err) {
      res.json({success: false, msg:'Failed to delete user'});
    } else {
      res.json({success: true, msg:'User deleted'});
    }
  });

});

router.post('/enableUser', passport.authenticate('jwt', {session:false}), function(req, res, next) {

  const userId = req.body.userId;

  User.enableUser(userId, function(err, user) {
    if(err) {
      res.json({success: false, msg:'Failed to enable user'});
    } else {
      res.json({success: true, msg:'User enabled'});
    }
  });

});



module.exports = router;
