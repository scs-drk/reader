const express = require('express');
const router = express.Router();
const passport = require('passport');
const config = require('../config/database');

const RSS = require('../models/rss');


// getAllEntries
router.get('/getAllEntries', passport.authenticate('jwt', {session:false}), function(req, res, next) {
  RSS.getAllEntries(function(err, rss) {
    if(err) {
      res.json({success: false, msg:'Could not get entries'});
    } else {
      res.json({rss:rss});
    }
  });
});

router.post('/getEntries', passport.authenticate('jwt', {session: false}), function(req, res, next) {
  let dismissed = req.body.dismissed;
  let relevant = req.body.relevant;
  let collected = req.body.collected;
  let category = req.body.category;

  RSS.getEntries(dismissed, relevant, collected, category, function(err, rss) {
    if(err) {
      res.json({success: false, msg:'Could not get entries'});
    } else {
      res.json({rss:rss});
    }
  });
});

// markAsDismissed
router.post('/markAsDismissed', passport.authenticate('jwt', {session:false}), function(req, res, next) {
  const entry_id = req.body.entry_id;
  //console.log("entry_id: " + entry_id);
  RSS.markAsDismissed(entry_id, function(err, rss) {
    if(err) {
      res.json({success: false, msg:'Could not be dismissed'});
    } else {
      res.json({success: true, msg:'Entry marked as dismissed'});
    }
  });
});

// markAsRelevant
router.post('/markAsRelevant', passport.authenticate('jwt', {session:false}), function(req, res, next) {
  const entry_id = req.body.entry_id;
  //console.log("entry_id: " + entry_id);
  RSS.markAsRelevant(entry_id, function(err, rss) {
    if(err) {
      res.json({success: false, msg:'Could not be marked as relevant'});
    } else {
      res.json({success: true, msg:'Entry marked as relevant'});
    }
  });
});

router.post('/addEntry', function(req, res, next) {
  let newRss = new RSS({
    title: req.body.title,
    link: req.body.link,
    description: req.body.description,
    pubDate: req.body.pubDate,
    author: req.body.author,
    source: req.body.source,
    category: req.body.category,
    dismissed: req.body.dismissed,
    dismissedDate: req.body.dismissedDate,
    relevant: req.body.relevant,
    relevantDate: req.body.relevantDate
  });

  RSS.addEntry(newRss, function(err, rss) {
    if(err) {
      res.json({success: false, msg:'Failed to add entry'});
    } else {
      res.json({success: true, msg:'Entry added successfully'});
    }
  });

});

module.exports = router;
